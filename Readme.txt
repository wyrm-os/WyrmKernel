Wyrm is an operating system microkernel.

The final goal of the project is to provide a userspace library to enable development of an actual operating system
on top of the kernel.

Environment setup:
1- Install the following packages before building the Toolchain and Kernel:

nasm qemu texinfo gcc-4.8 g++-4.8 make bison flex libgmp10 libgmp3-dev libmpfr4 libmpfr-dev libmpc3 libmpc3-dev

2- Build the Toolchain:

Make sure you have enough RAM, 1024 MB should be enough, compiling gcc takes A LOT of time and space depending on your hardware.
The make script will build binutils and gcc as a cross-compiler from your current architecture to a generic x86-64 architecture.
The script uses the gcc source uploaded to Wyrm's git repo. This source will be removed in the future and the checkout will be automatized.
The toolchain will be installed by default in ~/opt/cross. If you need to change that path, change it in both the PREFIX and WYRM_CROSS_COMPILER_PATH variables in the Toolchain/configure.sh and RunThisBeforeCompiling.sh files respectively.

  user@linux:$ cd Toolchain
  user@linux:$ . ./configure
  user@linux:$ make all

3- Build the Kernel:

  user@linux:$ cd ..
  user@linux:$ . ./RunThisBeforeCompiling
  user@linux:$ make all
  user@linux:$ ./run.sh

It should build and then run the Kernel in qemu