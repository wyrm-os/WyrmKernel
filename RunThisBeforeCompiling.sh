#!/bin/bash

echo "execute this script like this, with the two dots:"
echo "user@linux$ . ./RunThisBeforeCompiling.sh"

export WYRM_CROSS_COMPILER_PATH="$HOME/opt/cross/bin"
export WYRM_LIB_PATH="$HOME/opt/cross/lib"
export WYRM_PATH=`pwd`/Kernel

echo "environment variables exported"
