GCC=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-gcc
GPP=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-g++
LD=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-ld
AR=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-ar
ASM=nasm

GPPFLAGS=-fno-exceptions -fno-rtti -Wall -ffreestanding -nostdlib -fno-common -mno-red-zone -fno-asynchronous-unwind-tables -mno-mmx -mno-sse -mno-sse2 -fno-builtin-malloc -fno-builtin-free -fno-builtin-realloc
GCCFLAGS=-std=c99 -Wall -ffreestanding -nostdlib -fno-common -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -fno-builtin-malloc -fno-builtin-free -fno-builtin-realloc
HOSTGPPFLAGS=-I$(WYRM_PATH)/Collections -I$(WYRM_PATH)/StandardLibrary
ARFLAGS=rvs
ASMFLAGS=-felf64
