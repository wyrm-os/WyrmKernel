@set version-GCC 4.8.2
@clear DEVELOPMENT
@set srcdir /media/psf/Home/Projects/C-C++/Wyrm/Toolchain/gcc-build/gcc/../../sources/gcc-4.8.2/gcc
@set VERSION_PACKAGE (GCC) 
@set BUGURL @uref{http://gcc.gnu.org/bugs.html}
