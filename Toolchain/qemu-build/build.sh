#!/bin/bash

(cd "../sources/qemu-2.3.0" && \
./configure \
              --prefix="$PREFIX"\
              --target-list="x86_64-softmmu" && \
make &&
make install)
