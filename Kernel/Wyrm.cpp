#include <stdint.h>
#include <string.h>
#include "Version.h"
#include "Log/WyrmLog.h"
#include "MemoryManager/AllocatorSelector.h"
#include "MemoryManager/MemoryMap.h"
#include "MemoryManager/PageAllocator.h"
#include "VirtualMemoryManager/AddressSpaceManager.h"
#include "VirtualMemoryManager/AddressSpace.h"
#include "Scheduler/Timer.h"
#include "CPU/cpu.h"
#include "CPU/irqHandlers.h"
#include "CPU/InterruptTable.h"
#include "Scheduler/Scheduler.h"
#include "Scheduler/Process.h"
#include "DebugLibrary/Debug.h"
#include "PayloadUnpacker.h"
#include "Collections/SharedPointer.h"

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelsBinary;
extern uint8_t endOfKernel;

static const uint16_t PIC1RemapOffset = 0x20;
static const uint16_t PIC2RemapOffset = 0x28;
static const uint64_t kernelStackSize = PageAllocator::PageSize * 4;
static const uint64_t payloadSize = 0x40000;
static const void * payloadRelocAddress = (void*)0x2000000;
static void * userspaceStart = (void*)0xFFFF800000000000;

void configurePageAllocator(MemoryMap & memoryMap, PageAllocator & allocator);
void logKernelData(Log & log);
void loadProcesses(const void * payload, PageAllocator & pageAllocator, AddressSpaceManager & addressSpaceManager,
	Scheduler & scheduler, Log & log);
class A {};
extern "C" {
	void clearBSS(void * bssAddress, uint64_t bssSize) {
		memset(bssAddress, 0, bssSize);
	}

	void * getStackBase() {
		return (void*)(
			(uint64_t)&endOfKernel		//End of kernel
			+ PageAllocator::PageSize	//Dead zone used to page fault if there is an stack overflow
			+ kernelStackSize			//The size of the stack itself
			- sizeof(uint64_t)			//Begin at the top of the stack
		);
	}

	void * initializeKernelBinary() {
		//Relocate payload
		memcpy((void*)payloadRelocAddress, &endOfKernelsBinary, payloadSize);
		//Clear BSS
		clearBSS(&bss, &endOfKernel - &bss);
		//Return stack address
		return getStackBase();
	}
}

int main() {
	WyrmLog log;
	log.clear();

	logKernelData(log);

	MemoryMap map(log, (void*)((uint64_t)getStackBase() + sizeof(uint64_t)));

	PageAllocator pageAllocator(map.startAddress(), map.totalMemory(), log);
	configurePageAllocator(map, pageAllocator);

	AddressSpaceManager mapper(pageAllocator);

	AllocatorSelector allocatorSelector(pageAllocator, log);

	disableInterrupts();
	InterruptTable interruptTable((void*)0x0, PIC1RemapOffset, PIC2RemapOffset);
	interruptTable.setHandler(0x20, irq0Handler).setIRQMask(0, false);

	Scheduler scheduler(log);
	loadProcesses(payloadRelocAddress, pageAllocator, mapper, scheduler, log);

	log.str("Kernel md5sum: ").md5sum(&text, &endOfKernelsBinary - &text).newline();
	log.str("Startup finished.").newline();

	finishStartup();
	log.str("This should never be executed, if it is, you are in front of a terrible bug.").newline();

	return 0;
}

Log * plog;

class B {
public:
	int a;
	int * b;
};

class C {
private:
	int * a;
};

void loadProcesses(const void * payload, PageAllocator & pageAllocator, AddressSpaceManager & addressSpaceManager,
	Scheduler & scheduler, Log & log) {
	plog = &log;
	PayloadUnpacker payloadUnpacker(payload, pageAllocator, log);
	uint64_t a = 0x3000000;
	void * processText;
	uint32_t processSize;


	while ((processText = payloadUnpacker.loadNext(processSize)) != nullptr) {
		//SharedPointer<AddressSpace> addressSpace (new AddressSpace(userspaceStart, processSize / PageAllocator::PageSize, pageAllocator));
		//TODO: there's a weird error just before a delete, test by creating and deleting objects of the same size as
		// SharedPointer and SharedPointerContainer.
		// It happens when the size is 96 bytes

//		A * a = new A();
		B * b = new B();
//		C * c = new C();
//		delete c;
		delete b;
//		delete a;


//		SharedPointer<A> s(new A());
		AddressSpace addressSpace(userspaceStart, processSize / PageAllocator::PageSize, pageAllocator);
//		addressSpaceManager.map(addressSpace);
		
		//TODO: Hack until paging is properly set up
	//	memcpy((void*)userspaceStart, processText, processSize);
		memcpy((void*)(a += 0x100000), processText, processSize);
		log.str("Process md5sum: ").md5sum(processText, processSize).newline();
		Process * process = new Process((void*)a, pageAllocator, log);
		scheduler.addProcess(process);
	}

}

void configurePageAllocator(MemoryMap & memoryMap, PageAllocator & allocator) {
	for (unsigned int i = 1; i < memoryMap.count(); i++) {
		uint8_t * gapStart = (uint8_t*)memoryMap[i - 1].address + memoryMap[i - 1].size;
		uint8_t * gapEnd = (uint8_t*)memoryMap[i].address;

		if (gapStart < gapEnd) {
			allocator.blockSection(gapStart, gapEnd);
		}
	}
}

void logKernelData(Log & log) {
	log.str(Version::KernelName).str(" ");
	log.dec(Version::Mayor).chr('.').dec(Version::Minor).chr(' ');
	log.chr('\'').str(Version::ReleaseName).chr('\'');
	log.chr(' ').str(Version::Arch);
	log.newline();

	log.str("Alrighty then.").newline();
	log.str("Picture this if you will...").newline();
	log.newline();

	log.str("Kernel md5sum: ").md5sum(&text, &endOfKernelsBinary - &text).newline();
	log.str("text: ").addr(&text).newline();
	log.str("rodata: ").addr(&rodata).newline();
	log.str("data: ").addr(&data).newline();
	log.str("bss: ").addr(&bss).newline();
	log.str("kernel end: ").addr((void*)&endOfKernel).newline();
	log.str("stack's bottom: ").addr((void*)getStackBase()).newline();
	log.str("stack's top: ").addr(&endOfKernel + PageAllocator::PageSize).newline().newline();
}
