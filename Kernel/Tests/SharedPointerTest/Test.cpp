#include "../../Collections/SharedPointer.h"
#include <iostream>
#include <string>

using namespace std;

class A {
private:
	string id;

public:
	A(string id) {
		this->id = id;
		cout << "ctor A " << id << endl;
	}
	
	~A() { cout << "dtor A " << id << endl; }
};

int main(int argc, char ** argv) {
	cout << "Scope test" << endl;

	cout << "{" << endl;
	{
		SharedPointer<A> a(new A("1"));
	}
	cout << "}" << endl << endl;

	cout << "Getting pointer out of scope test" << endl;
	SharedPointer<A> b;

	cout << "{" << endl;
	{
		SharedPointer<A> c(new A("2"));
		b = c;
	}
	cout << "}" << endl << endl;


	cout << "Replacing pointer test" << endl;
	SharedPointer<A> d(new A("3"));
	SharedPointer<A> e(new A("4"));
	d = e;
	cout << endl;

	cout << "Finishing tests" << endl;
}
