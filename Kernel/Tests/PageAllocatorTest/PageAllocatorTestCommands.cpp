#include <string>
#include <iostream>
#include "../../MemoryManager/PageAllocator.h"
#include "PageAllocatorTestCommands.h"

NewAllocatorCommand::NewAllocatorCommand(Test & t) : Command("new allocator", "n", "usage: n <managed pages count>"), test(t) {
}

void NewAllocatorCommand::execute(CommandArguments & arguments) {
	int pageCount = 0;

	if (!arguments.get(pageCount).getSuccess())
		return;

	test.newAllocator(pageCount);
}

AllocCommand::AllocCommand(Test & t) :
	Command("alloc memory", "a", "usage: a <page count>"),
	test(t) {
}

void AllocCommand::execute(CommandArguments & arguments) {
	int pageCount = 0;

	if (!arguments.get(pageCount).getSuccess()) {
		return;
	}

	void * allocated = test.alloc(pageCount);

	if (allocated == NULL)
		cout << "Not enough free memory" << endl;
	else
		cout << "Allocated " << pageCount << " page(s) at " << allocated << endl;
}

AllocAtCommand::AllocAtCommand(Test & t) :
	Command("alloc memory at a speccific address", "A", "usage: A <address> <page count>"),
	test(t) {
}

void AllocAtCommand::execute(CommandArguments & arguments) {
	int pageCount = 0;
	void * address = NULL;

	if (!arguments.get(pageCount).get(address).getSuccess())
		return;

	void * allocated = test.alloc(address, pageCount);
	if (allocated == NULL)
		cout << "Not enough free memory" << endl;
	else
		cout << "Allocated " << pageCount << " page(s) at " << address << endl;
}

FreeCommand::FreeCommand(Test & t) :
	Command("free memory", "f", "usage: f <address>"),
	test(t) {
}

void FreeCommand::execute(CommandArguments & arguments) {
	void * address = NULL;

	if (!arguments.get(address).getSuccess())
		return;

	test.free(address);
}

BlockCommand::BlockCommand(Test & t) :
	Command("block memory", "b", "usage: f <from address> <to address>"),
	test(t) {
}

void BlockCommand::execute(CommandArguments & arguments) {
	void * addressFrom = NULL;
	void * addressTo = NULL;

	if (!arguments.get(addressFrom).get(addressTo).getSuccess())
		return;

	test.blockSection(addressFrom, addressTo);
}

PrintMapCommand::PrintMapCommand(Test & t) :
	Command("toggle map printing", "p", "usage: p"),
	test(t) {
}

void PrintMapCommand::execute(CommandArguments & arguments) {
	test.togglePrintMap();
}
