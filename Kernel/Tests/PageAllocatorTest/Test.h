#ifndef TEST_H
#define TEST_H

#include <string>
#include <stdint.h>
#include "TreePrinter.h"
#include "../../Shell/BaseShell.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../Log/TestLog.h"

using namespace std;

class Test : public BaseShell {
private:
	static const int PageSize = 4096;
	static const int PageCharWidth = 4;

	PageAllocator * allocator;
	TreePrinter * treePrinter;
	TestLog log;
	bool printMapEnabled;

public:
	Test();
	string getShellName();
	void run();
	void newAllocator(int managedMemoryInPages);
	void afterCommandExecution();
	void * alloc(int count);
	void * alloc(void * address, int count);
	void free(void * address);
	void blockSection(void * addressFrom, void * addressTo);
	void togglePrintMap();

private:
	void printMap();
	void printNodeRow(uint8_t * table, int & current, int nodeCount, int nodeWidth);
	void printNode(int width, bool partialOrRequested, bool complete);
};

#endif