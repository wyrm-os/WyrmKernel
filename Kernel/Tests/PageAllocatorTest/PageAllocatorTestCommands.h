#include <string>
#include <iostream>
#include "Test.h"
#include "../../Shell/Command.h"
#include "../../MemoryManager/PageAllocator.h"

class NewAllocatorCommand : public Command {
	Test & test;

public:
	NewAllocatorCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class AllocCommand : public Command {
	Test & test;

public:
	AllocCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class AllocAtCommand : public Command {
	Test & test;

public:
	AllocAtCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class FreeCommand : public Command {
	Test & test;

public:
	FreeCommand(Test & t);

	void execute(CommandArguments & arguments);
};

class BlockCommand : public Command {
	Test & test;

public:
	BlockCommand(Test & t); 
	void execute(CommandArguments & arguments);
};

class PrintMapCommand : public Command {
	Test & test;

public:
	PrintMapCommand(Test & t);
	void execute(CommandArguments & arguments);
};
