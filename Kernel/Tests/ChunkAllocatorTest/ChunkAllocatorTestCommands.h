#ifndef CHUNK_ALLOCATOR_TEST_COMMANDS_H
#define CHUNK_ALLOCATOR_TEST_COMMANDS_H

#include <string>
#include <iostream>
#include "Test.h"
#include "../../Shell/Command.h"
#include "../../MemoryManager/ChunkAllocator.h"

class NewAllocatorCommand : public Command {
	Test & test;

public:
	NewAllocatorCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class AllocCommand : public Command {
	Test & test;

public:
	AllocCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class FreeCommand : public Command {
	Test & test;

public:
	FreeCommand(Test & test);

	void execute(CommandArguments & arguments);
};

class StressCommand : public Command {
	Test & test;

public:
	StressCommand(Test & test); 
	void execute(CommandArguments & arguments);
};

class PrintTreeCommand : public Command {
	Test & test;

public:
	PrintTreeCommand(Test & t);
	void execute(CommandArguments & arguments);
};

#endif