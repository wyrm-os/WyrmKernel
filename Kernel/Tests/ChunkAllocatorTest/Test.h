#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <string>
#include <stdint.h>
#include <list>
#include "../RedBlackTreeTest/RedBlackTreeVerifier.h"
#include "../PageAllocatorTest/TreePrinter.h"
#include "../../MemoryManager/Chunk.h"
#include "../../Shell/BaseShell.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../MemoryManager/ChunkAllocator.h"
#include "../../Log/TestLog.h"

using namespace std;

ostream& operator<< (ostream& stream, const Chunk & chunk);

class Test : public BaseShell {
private:
	static const int PageSize = 4096;

	void * mem;
	PageAllocator * pageAllocator;
	ChunkAllocator * chunkAllocator;
	RedBlackTreeVerifier<Chunk> * verifier;
	TreePrinter * treePrinter;
	TestLog log;
	list<void*> allocated;
	bool printTree;

public:
	Test();
	string getShellName();
	void run();
	void newAllocator(int managedMemoryInPages, int chunkSize);
	void * alloc();
	void dealloc(void * address);
	void afterCommandExecution();
	void stressTest(int totalAllocatedChunks, int chunkSize);
	void togglePrintTree();

private:
	void * randomElement(vector<void*> & allocated);
};

#endif