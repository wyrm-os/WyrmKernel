#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include "Test.h"
#include "ChunkAllocatorTestCommands.h"
#include "../RedBlackTreeTest/RedBlackTreeVerifier.h"
#include "../../Shell/BaseShell.h"
#include "../../Collections/BitArray.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../MemoryManager/ChunkAllocator.h"
#include "../../MemoryManager/Chunk.h"
#include "../../Log/TestLog.h"

using namespace std;

ostream& operator<< (ostream& stream, const Chunk & chunk) {
	return stream << " count: " << chunk.objectCount;
}

Test::Test() {
	mem = NULL;
	newAllocator(16, 512);
	printTree = false;
}

string Test::getShellName() {
	return "chunkAllocator";
}

void Test::run() {
	vector<Command*> commands;
	
	NewAllocatorCommand newAllocator(*this);
	AllocCommand alloc(*this);
	FreeCommand free(*this);
	StressCommand stress(*this);
	PrintTreeCommand printTree(*this);

	commands.push_back(&newAllocator);
	commands.push_back(&alloc);
	commands.push_back(&free);
	commands.push_back(&stress);
	commands.push_back(&printTree);

	BaseShell::run(&commands);
}

void Test::newAllocator(int managedMemoryInPages, int chunkSize) {
	if (mem != NULL) {
		free(mem);
		allocated.clear();
		delete pageAllocator;
		delete chunkAllocator;
		delete verifier;
		delete treePrinter;
	}

	mem = malloc(managedMemoryInPages * PageSize);

	if (mem == NULL) {
		cout << "Not enough memory" << endl;
		return;
	}

	pageAllocator = new PageAllocator(mem, managedMemoryInPages * PageSize, log);
	chunkAllocator = new ChunkAllocator(*pageAllocator, chunkSize, log);
	verifier = new RedBlackTreeVerifier<Chunk>(chunkAllocator->getFreeChunksTree());
	treePrinter = new TreePrinter(*pageAllocator);
}

void * Test::alloc() {
	void * mem = chunkAllocator->alloc();

	if (mem != NULL)
		allocated.push_back(mem);

	return mem;
}

void Test::dealloc(void * address) {
	allocated.remove(address);
	chunkAllocator->dealloc(address);
}

void Test::afterCommandExecution() {
	if (printTree) {
		treePrinter->printMap();
		verifier->print();
		verifier->printInOrder();
		cout << "Property 2: " << (verifier->checkProperty2()? "y" : "n") << endl;
		cout << "Property 4: " << (verifier->checkProperty4()? "y" : "n") << endl;
		cout << "Property 5: " << (verifier->checkProperty5()? "y" : "n") << endl;
	}

	cout << "Allocated: ";

	for (list<void*>::iterator it = allocated.begin(); it != allocated.end(); ++it)
  		cout << *it << ' ';

	cout << endl;
}

void Test::stressTest(int totalChunks, int chunkSize) {
	int totalAllocs = 2 * totalChunks;
	int totalFrees = totalChunks;
	int currentAllocs = 0;
	int currentFrees = 0;

	srand(totalChunks);
	PageAllocator * pageAllocator = new PageAllocator(mem, totalChunks * PageSize, log);
	ChunkAllocator * chunkAllocator = new ChunkAllocator(*pageAllocator, chunkSize, log);
	RedBlackTreeVerifier<Chunk> * verifier = new RedBlackTreeVerifier<Chunk>(chunkAllocator->getFreeChunksTree());
	TreePrinter * treePrinter = new TreePrinter(*pageAllocator);
	vector<void*> allocated;

	log.str("Randomly allocating chunks").newline();
	log.str("Operations: ");
	while(currentAllocs < totalAllocs) {
		//Test for a forced alloc
		if (currentFrees == totalFrees ||
			currentFrees == currentAllocs) {
			allocated.push_back(chunkAllocator->alloc());
			log.str("A");
			currentAllocs++;
		//Test for a forced dealloc
		} else if (currentAllocs - currentFrees == totalChunks){
			chunkAllocator->dealloc(randomElement(allocated));
			log.str("F");
			currentFrees++;
		} else if (rand() % 2) {
			allocated.push_back(chunkAllocator->alloc());
			log.str("A");
			currentAllocs++;
		} else {
			chunkAllocator->dealloc(randomElement(allocated));
			log.str("F");
			currentFrees++;
		}
	}

	log.newline();
	log.str("All chunks allocated").newline();
	treePrinter->printMap();
	verifier->print();
	cout << "Property 2: " << (verifier->checkProperty2()? "y" : "n") << endl;
	cout << "Property 4: " << (verifier->checkProperty4()? "y" : "n") << endl;
	cout << "Property 5: " << (verifier->checkProperty5()? "y" : "n") << endl;

	while (allocated.size() > 0) {
		chunkAllocator->dealloc(randomElement(allocated));
	}

	log.str("All chunks deallocated").newline();
	treePrinter->printMap();
	verifier->print();
	cout << "Property 2: " << (verifier->checkProperty2()? "y" : "n") << endl;
	cout << "Property 4: " << (verifier->checkProperty4()? "y" : "n") << endl;
	cout << "Property 5: " << (verifier->checkProperty5()? "y" : "n") << endl;

	delete pageAllocator;
	delete chunkAllocator;
	delete verifier;
	delete treePrinter;
}

void * Test::randomElement(vector<void*> & allocated) {
	int index = rand() % allocated.size();
	void * result = allocated[index];
	allocated.erase (allocated.begin() + index);
	return result;
}

void Test::togglePrintTree() {
	printTree = !printTree;
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}