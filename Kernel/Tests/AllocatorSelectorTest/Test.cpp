#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stdio.h>
#include <stdlib.h>
#include "Test.h"
#include "AllocatorSelectorTestCommands.h"
#include "../../Shell/BaseShell.h"
#include "../../MemoryManager/AllocatorSelector.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../Log/TestLog.h"

using namespace std;

Test::Test() {
	mem = NULL;
	newAllocator(512);
}

string Test::getShellName() {
	return "allocatorSelector";
}

void Test::run() {
	vector<Command*> commands;

	NewAllocatorCommand newAllocator(*this);
	AllocCommand alloc(*this);
	FreeCommand free(*this);
	AllocDeallocCommand allocDeallocCommand(*this);

	commands.push_back(&newAllocator);
	commands.push_back(&alloc);
	commands.push_back(&free);
	commands.push_back(&allocDeallocCommand);

	BaseShell::run(&commands);
}

void Test::newAllocator(int managedMemoryInPages) {
	if (mem != NULL) {
		free(mem);
		allocated.clear();
		delete pageAllocator;
		delete allocatorSelector;
	}

	mem = malloc(managedMemoryInPages * PageSize);

	if (mem == NULL) {
		cout << "Not enough memory" << endl;
		return;
	}

	pageAllocator = new PageAllocator(mem, managedMemoryInPages * PageSize, log);
	allocatorSelector = new AllocatorSelector(*pageAllocator, log);
}

void * Test::alloc(int size) {
	void * mem = AllocatorSelector::newImpl(size);

	if (mem != NULL)
		allocated[mem] = size;

	return mem;
}

void Test::dealloc(void * address) {
	map<void*, int>::iterator i = allocated.find(address);

	if (i != allocated.end()) {
		AllocatorSelector::deleteImpl(address);
		allocated.erase(address);
	}
}

void Test::afterCommandExecution() {
	cout << "Allocated: ";

	for (map<void*, int>::iterator it = allocated.begin(); it != allocated.end(); ++it)
  		cout << it->first << '(' << it->second << "b) ";

	cout << endl;
}

void Test::stressTest(int totalChunks, int chunkSize) {
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}