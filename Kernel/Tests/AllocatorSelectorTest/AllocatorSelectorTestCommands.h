#ifndef ALLOCATOR_SELECTOR_TEST_COMMANDS_H
#define ALLOCATOR_SELECTOR_TEST_COMMANDS_H

#include <string>
#include <iostream>
#include "Test.h"
#include "../../Shell/Command.h"

class NewAllocatorCommand : public Command {
	Test & test;

public:
	NewAllocatorCommand(Test & t);
	void execute(CommandArguments & arguments);
};

class AllocCommand : public Command {
	Test & test;

public:
	AllocCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class FreeCommand : public Command {
	Test & test;

public:
	FreeCommand(Test & test);

	void execute(CommandArguments & arguments);
};

class AllocDeallocCommand : public Command {
	Test & test;

public:
	AllocDeallocCommand(Test & test);
	void execute(CommandArguments & arguments);
};

#endif