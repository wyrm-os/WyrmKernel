#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <stdint.h>
#include <map>
#include "../../Shell/BaseShell.h"
#include "../../MemoryManager/PageAllocator.h"
#include "../../MemoryManager/AllocatorSelector.h"
#include "../../Log/TestLog.h"

using namespace std;

class Test : public BaseShell {
private:
	static const int PageSize = 4096;

	void * mem;
	AllocatorSelector * allocatorSelector;
	PageAllocator * pageAllocator;
	TestLog log;
	map<void*, int> allocated;

public:
	Test();
	string getShellName();
	void run();
	void newAllocator(int managedMemoryInPages);
	void * alloc(int size);
	void dealloc(void * address);
	void afterCommandExecution();
	void stressTest(int totalAllocatedChunks, int chunkSize);
	void togglePrintTree();
};

#endif