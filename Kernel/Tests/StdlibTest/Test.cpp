#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include "Test.h"
#include "StdlibTestCommands.h"
#include "../Shell/BaseShell.h"
#include "../Shell/Command.h"
#include "../StandardLibrary/stdlib.h"

void Test::run() {
	vector<Command*> commands;
	SortCommand sort;
	commands.push_back(&sort);
	BaseShell::run(&commands);
}

string Test::getShellName() {
	return "Test";
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}
