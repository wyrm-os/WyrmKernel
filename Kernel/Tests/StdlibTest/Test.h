#ifndef TEST_H
#define TEST_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include <vector>
#include "../Shell/BaseShell.h"
#include "../Shell/Command.h"
#include "../StandardLibrary/stdlib.h"

class Test : BaseShell {
public:
	void run();
	string getShellName();
};

#endif