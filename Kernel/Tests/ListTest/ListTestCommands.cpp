#include "ListTestCommands.h"
#include "Test.h"

NewListCommand::NewListCommand(Test & t) :
	Command("new list", "n", "usage: n"),
	test(t) {
}

void NewListCommand::execute(CommandArguments & arguments) {
	if (!arguments.getSuccess())
		return;

	test.newList();
}

PrintCommand::PrintCommand(Test & t) :
	Command("toggle print", "p", "usage: p"),
	test(t) {
}

void PrintCommand::execute(CommandArguments & arguments) {
	if (!arguments.getSuccess())
		return;

	test.togglePrint();
}

ReversePrintCommand::ReversePrintCommand(Test & t) :
	Command("toggle reverse print", "r", "usage: r"),
	test(t) {
}

void ReversePrintCommand::execute(CommandArguments & arguments) {
	if (!arguments.getSuccess())
		return;

	test.toggleReversePrint();
}

PushBackCommand::PushBackCommand(Test & t) :
	Command("push back", "pb", "usage: pb <element>"),
	test(t) {
}

void PushBackCommand::execute(CommandArguments & arguments) {
	int value;

	if (!arguments.get(value).getSuccess())
		return;

	test.pushBack(value);
}

PushFrontCommand::PushFrontCommand(Test & t) :
	Command("push front", "pf", "usage: pf <element>"),
	test(t) {
}

void PushFrontCommand::execute(CommandArguments & arguments) {
	int value;

	if (!arguments.get(value).getSuccess())
		return;

	test.pushFront(value);
}

PopBackCommand::PopBackCommand(Test & t) :
	Command("pop back", "ppb", "usage: ppb"),
	test(t) {
}

void PopBackCommand::execute(CommandArguments & arguments) {
	if (!arguments.getSuccess())
		return;

	test.popBack();
}

PopFrontCommand::PopFrontCommand(Test & t) :
	Command("pop front", "ppf", "usage: ppf"),
	test(t) {
}

void PopFrontCommand::execute(CommandArguments & arguments) {
	if (!arguments.getSuccess())
		return;

	test.popFront();
}

ClearCommand::ClearCommand(Test & t) :
	Command("clear", "c", "usage: c"),
	test(t) {
}

void ClearCommand::execute(CommandArguments & arguments) {
	test.clear();
}

IteratorNextCommand::IteratorNextCommand(Test & t) :
	Command("next element", "nxt", "usage: nxt"),
	test(t) {
}

void IteratorNextCommand::execute(CommandArguments & arguments) {
	test.iteratorNext();
}

IteratorPrevCommand::IteratorPrevCommand(Test & t) :
	Command("previous element", "prv", "usage: prv"),
	test(t) {
}

void IteratorPrevCommand::execute(CommandArguments & arguments) {
	test.iteratorPrev();
}

InsertCommand::InsertCommand(Test & t) :
	Command("insert element", "i", "usage: i <element>"),
	test(t) {
}

void InsertCommand::execute(CommandArguments & arguments) {
	int element = 0;

	if (!arguments.get(element).getSuccess())
		return;

	test.insert(element);
}

RemoveCommand::RemoveCommand(Test & t) :
	Command("remove element", "rm", "usage: rm"),
	test(t) {
}

void RemoveCommand::execute(CommandArguments & arguments) {
	test.remove();
}
