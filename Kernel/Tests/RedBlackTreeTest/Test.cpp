#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Test.h"
#include "RedBlackTreeTestCommands.h"
#include "../../Shell/BaseShell.h"
#include "../../Log/TestLog.h"

using namespace std;

Test::Test() {
	tree = NULL;
	newTree();
	printTreeEnabled = false;
}

string Test::getShellName() {
	return "redBlackTree";
}

void Test::run() {
	vector<Command*> commands;
	
	NewTreeCommand newTree(*this);
	InsertCommand insert(*this);
	RemoveCommand remove(*this);
	MaxCommand max(*this);
	MaxCapCommand maxcap(*this);
	MinCommand min(*this);
	MinCapCommand mincap(*this);
	StressTestCommand stressTest(*this);
	TogglePrintCommand togglePrint(*this);

	commands.push_back(&newTree);
	commands.push_back(&insert);
	commands.push_back(&remove);
	commands.push_back(&max);
	commands.push_back(&maxcap);
	commands.push_back(&min);
	commands.push_back(&mincap);
	commands.push_back(&stressTest);
	commands.push_back(&togglePrint);

	BaseShell::run(&commands);
}

void Test::newTree() {
	if (tree != NULL) {
		delete tree;
		delete verifier;
	} 

	tree = new RedBlackTree<int>();
	verifier = new RedBlackTreeVerifier<int>(*tree);
}

void Test::afterCommandExecution() {
	if (printTreeEnabled) {
		verifier->print();
	}
}

void Test::insert(int value) {
	tree->insert(createNode(value));
}

void Test::remove(int value) {
	tree->remove(tree->search(value));
}

void Test::max() {
	RedBlackTreeNode<int> * root = tree->getRoot();
	RedBlackTreeNode<int> * node = root? root->max() : NULL;
	cout << "max: [" << node << "] ";

	if (node)
		cout << node->getElement();

	cout << endl;
}

void Test::max(int cap) {
	RedBlackTreeNode<int> * root = tree->getRoot();
	RedBlackTreeNode<int> * node = root ? root->max(cap) : NULL;
	cout << "[" << node << "] ";

	if (node)
		cout << node->getElement();

	cout << endl;
}

void Test::min() {
	RedBlackTreeNode<int> * root = tree->getRoot();
	RedBlackTreeNode<int> * node = root? root->min() : NULL;
	cout << "min: [" << node << "] ";

	if (node)
		cout << node->getElement();

	cout << endl;
}

void Test::min(int cap) {
	RedBlackTreeNode<int> * root = tree->getRoot();
	RedBlackTreeNode<int> * node = root ? root->min(cap) : NULL;
	cout << "[" << node << "] ";

	if (node)
		cout << node->getElement();

	cout << endl;
}

void Test::stressTest(int count) {
	RedBlackTree<int> * tree = new RedBlackTree<int>();
	RedBlackTreeVerifier<int> * verifier = new RedBlackTreeVerifier<int>(*tree);
	
	cout << "Generating test vectors" << endl;
	vector<int> insertTestVector;
	randomizeTestVector(initializeTestVector(insertTestVector, count), 0);

	vector<int> deleteTestVector;
	randomizeTestVector(initializeTestVector(deleteTestVector, count), 1);

	vector<RedBlackTreeNode<int>*> nodes;
	for (int i = 0; i < count; i++)
		nodes.push_back(createNode(insertTestVector[i]));

	cout << "Performing " << count << " insertions" << endl;
	for (int  i = 0; i < count; i++)
		tree->insert(nodes[i]);

	cout << "Property 2: " << (verifier->checkProperty2()? "yes" : "no") << endl;
	cout << "Property 4: " << (verifier->checkProperty4()? "yes" : "no") << endl;
	cout << "Property 5: " << (verifier->checkProperty5()? "yes" : "no") << endl;

	cout << "Performing " << count << " removals" << endl;
	for (int  i = 0; i < count; i++)
		tree->remove(tree->search(deleteTestVector[i]));

	cout << "Destroying" << endl;

	for (int i = 0; i < count; i++)
		free(nodes[i]);

	delete tree;
	delete verifier;

	cout << "Done" << endl;
}

void Test::togglePrintTree() {
	printTreeEnabled = !printTreeEnabled;
}

RedBlackTreeNode<int> * Test::createNode(int value) {
	return RedBlackTree<int>::createNodeFromMemoryChunk(
		malloc(RedBlackTree<int>::getNodeSize()),
		value
	);
}

vector<int> & Test::initializeTestVector(vector<int> & v, int count) {
	for (int i = 0; i < count; i++)
		v.push_back(i);

	return v;
}

vector<int> & Test::randomizeTestVector(vector<int> & v, int offset) {
	srand(v.size() + offset);
	for (int l = 0; l < v.size(); l++) {
		int r = l + rand() % (v.size() - l);
		int a = v[l];
		v[l] = v[r];
		v[r] = a;
	}

	return v;
}

int main(int argc, char ** argv) {
	Test test;
	test.run();
	return 0;
}