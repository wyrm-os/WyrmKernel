#ifndef RED_BLACK_TREE_TEST_COMMANDS
#define RED_BLACK_TREE_TEST_COMMANDS

#include "Test.h"
#include "../../Shell/Command.h"

class NewTreeCommand : public Command {
private:
	Test & test;

public:
	NewTreeCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class InsertCommand : public Command {
private:
	Test & test;

public:
	InsertCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class RemoveCommand : public Command {
private:
	Test & test;

public:
	RemoveCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class StressTestCommand : public Command {
private:
	Test & test;

public:
	StressTestCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class TogglePrintCommand : public Command {
private:
	Test & test;

public:
	TogglePrintCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class MaxCommand : public Command {
private:
	Test & test;

public:
	MaxCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class MaxCapCommand : public Command {
private:
	Test & test;

public:
	MaxCapCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class MinCommand : public Command {
private:
	Test & test;

public:
	MinCommand(Test & test);
	void execute(CommandArguments & arguments);
};

class MinCapCommand : public Command {
private:
	Test & test;

public:
	MinCapCommand(Test & test);
	void execute(CommandArguments & arguments);
};

#endif