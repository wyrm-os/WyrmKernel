#ifndef RED_BLACK_TREE_VERIFIER_H
#define RED_BLACK_TREE_VERIFIER_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <ctype.h>
#include "../../Collections/RedBlackTree.h"
#include "../../Collections/RedBlackTreeNode.h"
#include "../../Collections/RedBlackTreeNodeImpl.h"

using namespace std;

template<typename T>
class RedBlackTreeVerifier {
private:
	RedBlackTree<T> & tree;

public:
	RedBlackTreeVerifier(RedBlackTree<T> & tree);
	void print();
	void printInOrder();
	bool checkProperties();
	bool checkProperty2();
	bool checkProperty4();
	bool checkProperty5();

private:
	void print(RedBlackTreeNode<T> * node, int depth);
	bool checkProperty4(RedBlackTreeNode<T> * node);
	bool checkProperty5(RedBlackTreeNode<T> * node, int * blackCount);
};

template<typename T>
RedBlackTreeVerifier<T>::RedBlackTreeVerifier(RedBlackTree<T> & t) : tree(t) {
}

template<typename T>
void RedBlackTreeVerifier<T>::print() {
	print(tree.getRoot(), 0);
}

template<typename T>
void RedBlackTreeVerifier<T>::printInOrder() {
	RedBlackTreeNode<T> * current = tree.getRoot();

	if (current)
		current = current->min();

	while(current) {
		cout << current->getElement() << " ";
		current = current->inOrderNext();
	}

	cout << endl;
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperties() {
	int stub = 0;
	return checkProperty2() &&
		checkProperty4(tree.getRoot()) &&
		checkProperty5(tree.getRoot(), &stub);
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperty2() {
	RedBlackTreeNode<T> * root = tree.getRoot();
	return root == NULL || root->getColor() == RBTBlack;
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperty4() {
	checkProperty4(tree.getRoot());
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperty5() {
	int blackCount = 0;
	checkProperty5(tree.getRoot(), &blackCount);
}

template<typename T>
void RedBlackTreeVerifier<T>::print(RedBlackTreeNode<T> * node, int depth) {
	int tabs = depth;

	while(tabs--)
		cout << "  ";

	if (!node) {
		cout << "[null]" << endl;
		return;
	}

	cout << "[" << (void*)node << "]" << node->getElement();
	cout << " " << (node->getColor() == RBTRed? "R" : "B") << endl;

	print(node->getLeftChild(), depth + 1);
	print(node->getRightChild(), depth + 1);
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperty4(RedBlackTreeNode<T> * node) {
	if (node == NULL)
		return true;

	bool red = (node->getColor() == RBTRed);
	bool leftBlack = (!node->getLeftChild() || node->getLeftChild()->getColor() == RBTBlack);
	bool rightBlack = (!node->getRightChild() || node->getRightChild()->getColor() == RBTBlack);

	return (!red || (leftBlack && rightBlack)) &&
		checkProperty4(node->getLeftChild()) &&
		checkProperty4(node->getRightChild());
}

template<typename T>
bool RedBlackTreeVerifier<T>::checkProperty5(RedBlackTreeNode<T> * node, int * blackCount) {
	int leftBlackCount = 0;
	int rightBlackCount = 0;

	if (node == NULL) {
		*blackCount = 1;
		return true;
	}

	bool leftOk = checkProperty5(node->getLeftChild(), &leftBlackCount);

	if (!leftOk)
		return false;

	bool rightOk = checkProperty5(node->getRightChild(), &rightBlackCount);

	if (!rightOk)
		return false;

	*blackCount = leftBlackCount + (node->getColor() == RBTBlack? 1 : 0);

	return leftBlackCount == rightBlackCount;
}

#endif