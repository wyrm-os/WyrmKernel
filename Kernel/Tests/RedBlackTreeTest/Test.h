#ifndef TEST_H
#define TEST_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include "Test.h"
#include "RedBlackTreeVerifier.h"
#include "../../Shell/BaseShell.h"
#include "../../Collections/RedBlackTree.h"
#include "../../Log/TestLog.h"

using namespace std;

class Test : BaseShell {
private:
	RedBlackTree<int> * tree;
	RedBlackTreeVerifier<int> * verifier;
	bool printTreeEnabled;

public:
	Test();
	string getShellName();
	void run();
	void newTree();
	void insert(int value);
	void remove(int value);
	void max();
	void max(int cap);
	void min();
	void min(int cap);
	void stressTest(int count);
	void togglePrintTree();
	virtual void afterCommandExecution();

private:
	RedBlackTreeNode<int> * createNode(int value);
	vector<int> & initializeTestVector(vector<int> & v, int count);
	vector<int> & randomizeTestVector(vector<int> & v, int offset);
};

#endif