#ifndef LOG_TEST_COMMANDS_H
#define LOG_TEST_COMMANDS_H

#include <string>
#include <iostream>
#include "Test.h"
#include "../../Shell/Command.h"
#include "../../Log/Log.h"

class ClearCommand : public Command {
	Test & test;

public:
	ClearCommand(Test & t);
	void execute(CommandArguments & arguments);
};

#endif