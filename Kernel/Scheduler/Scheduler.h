#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "Process.h"
#include "ProcessSlot.h"

#ifdef __cplusplus
extern "C" {
#endif


void * switchUserToKernel(void * esp);
void * switchKernelToUser();
void * getCurrentEntryPoint();

#ifdef __cplusplus
}
#endif

class Scheduler {
public:
	ProcessSlot * current;

private:
	Log & log;

public:
	Scheduler(Log & log);
	void schedule();
	void addProcess(Process * process);
	void removeProcess(Process * process);
};

#endif