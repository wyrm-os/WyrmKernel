#include <stdlib.h>
#include <stdint.h>
#include "Process.h"
#include "../MemoryManager/PageAllocator.h"

struct StackFrame {
	//Registers restore context
	uint64_t gs;
	uint64_t fs;
	uint64_t r15;
	uint64_t r14;
	uint64_t r13;
	uint64_t r12;
	uint64_t r11;
	uint64_t r10;
	uint64_t r9;
	uint64_t r8;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rbp;
	uint64_t rdx;
	uint64_t rcx;
	uint64_t rbx;
	uint64_t rax;

	//iretq hook
	uint64_t rip;
	uint64_t cs;
	uint64_t eflags;
	uint64_t rsp;
	uint64_t ss;
	uint64_t base;
};

Process::Process(void * entryPoint, PageAllocator & pa, Log & l) : pageAllocator(pa), log(l) {
	userStackPage = pageAllocator.alloc(1);
	kernelStackPage = pageAllocator.alloc(1);
	this->entryPoint = entryPoint;

	userStack = toStackAddress(userStackPage);
	kernelStack = toStackAddress(kernelStackPage);
	userStack = fillStackFrame(entryPoint, userStack);

	log.str("Process created:").newline();
	log.str("Code entry point: ").addr(entryPoint).newline();
	log.str("Kernel stack: ").addr(kernelStack).str(" Page:").addr(kernelStackPage).newline();
	log.str("User stack: ").addr(userStack).str(" Page:").addr(userStackPage).newline().newline();
}

Process::~Process() {
	pageAllocator.dealloc(userStackPage);
	pageAllocator.dealloc(kernelStackPage);
}

void * Process::toStackAddress(void * page) {
	return (uint8_t*)page + PageAllocator::PageSize - 0x10;
}

void * Process::fillStackFrame(void * entryPoint, void * userStack) {
	StackFrame * frame = (StackFrame*)userStack - 1;
	frame->gs =		0x001;
	frame->fs =		0x002;
	frame->r15 =	0x003;
	frame->r14 =	0x004;
	frame->r13 =	0x005;
	frame->r12 =	0x006;
	frame->r11 =	0x007;
	frame->r10 =	0x008;
	frame->r9 =		0x009;
	frame->r8 =		0x00A;
	frame->rsi =	0x00B;
	frame->rdi =	0x00C;
	frame->rbp =	0x00D;
	frame->rdx =	0x00E;
	frame->rcx =	0x00F;
	frame->rbx =	0x010;
	frame->rax =	0x011;
	frame->rip =	(uint64_t)entryPoint;
	frame->cs =		0x008;
	frame->eflags = 0x202;
	frame->rsp =	(uint64_t)&(frame->base);
	frame->ss = 	0x000;
	frame->base =	0x000;

	return frame;
}
