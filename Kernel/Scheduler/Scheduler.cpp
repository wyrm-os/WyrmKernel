#include "Scheduler.h"
#include "../DebugLibrary/Debug.h"
static Scheduler * scheduler;

Log * logg;

void * switchUserToKernel(void * esp) {
	Process * process = scheduler->current->process;
	process->userStack = esp;
	return process->kernelStack;
}

void * switchKernelToUser() {
	scheduler->schedule();
//	logg->str("Process: ").addr(scheduler->current).newline();
	return scheduler->current->process->userStack;
}

Scheduler::Scheduler(Log & l) : log(l) {
	scheduler = this;
	current	= nullptr;
	logg = &log;
}

void * getCurrentEntryPoint() {
	return scheduler->current->process->entryPoint;
}

void Scheduler::schedule() {
	current = current->next;
}

void Scheduler::addProcess(Process * process) {
	ProcessSlot * newProcess = new ProcessSlot(process);

	if (current == nullptr) {
		current = newProcess;
		current->next = current;
	} else {
		ProcessSlot * next = current->next;
		current->next = newProcess;
		newProcess->next = next;
	}
}

//TODO Implement efficient deletion with a map
void Scheduler::removeProcess(Process * process) {
	ProcessSlot * prevSlot = current;
	ProcessSlot * slotToRemove = current->next;

	if (current == nullptr) {
		return;
	} else if (prevSlot == slotToRemove && process == current->process) {
		delete current;
		current = nullptr;
		return;
	}

	while(slotToRemove->process != process) {
		prevSlot = slotToRemove;
		slotToRemove = slotToRemove->next;
	}

	prevSlot->next = slotToRemove->next;
	delete slotToRemove;
}
