#include <stdint.h>
#include <string.h>
#include <mathutil.h>
#include "PayloadUnpacker.h"
#include "MemoryManager/PageAllocator.h"
#include "Log/Log.h"

PayloadUnpacker::PayloadUnpacker(const void * packedPayloadAddress, PageAllocator & pa, Log & l) :
	pageAllocator(pa), log(l) {
	readPointer = (void*)packedPayloadAddress;
	processCount = readUint32(readPointer);

	log.str("Payload Unpacker:").newline();
	log.str("Will unpack ").count(processCount).str(" payloads.").newline();
}

void * PayloadUnpacker::loadNext(uint32_t & size) {
	if (processCount-- == 0)
		return nullptr;

	size = readUint32(readPointer);
	void * pages = pageAllocator.alloc(upperDivide(size, PageAllocator::PageSize));
	readChunk(pages, size, readPointer);
	return pages;
}

uint32_t PayloadUnpacker::readUint32(void *& readPointer) {
	uint32_t result = *((uint32_t*)readPointer);
	readPointer = ((char*)readPointer) + sizeof(uint32_t);
	return result;
}

void PayloadUnpacker::readChunk(void * destination, uint32_t size, void *& readPointer) {
	memcpy(destination, readPointer, size);
	readPointer = ((char*)readPointer) + size;
}
