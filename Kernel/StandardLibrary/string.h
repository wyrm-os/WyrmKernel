#ifndef STRING_H
#define STRING_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void * memcpy(void * destination, const void * source, uint64_t length);
void * memset(void * destination, int32_t c, uint64_t length);

#ifdef __cplusplus
}
#endif

#endif