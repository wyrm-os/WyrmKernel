#include "mathutil.h"

uint64_t upperDivide(uint64_t n, uint64_t d) {
	return n / d + (n % d != 0? 1 : 0);
}

uint32_t nextHighestPowerOf2(uint32_t n) {
	n--;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n++;
	return n;	
}

uint64_t max(uint64_t e1, uint64_t e2) {
	return e1 > e2? e1 : e2;
}

uint64_t min(uint64_t e1, uint64_t e2) {
	return e1 < e2? e1 : e2;
}
