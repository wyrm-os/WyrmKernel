#ifndef STDLIB_H
#define STDLIB_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef int32_t(*comparer)(const void *, const void *);
void * memcpy(void * dst, const void * src, uint64_t len);
void qsort (void * base, uint64_t num, uint64_t size, int32_t (*compar)(const void *, const void *));

//The following are not implemented in the kernel, but the
//prototypes are present to be able to compille the test binaries
/*void * malloc(uint32_t);
void * realloc(void *, uint32_t);
void free(void *);
void srand(uint32_t);
uint32_t rand();*/
typedef int32_t (*__compar_fn_t)(const void *, const void *);

#ifdef __cplusplus
}
#endif

#endif
