#ifndef SETJMP_H
#define SETJMP_H

#define JBLEN 72

typedef int jmp_buf[JBLEN];


#ifdef __cplusplus
extern "C" {
#endif

int setjmp(jmp_buf);
void longjmp(jmp_buf, int);

#ifdef __cplusplus
}
#endif

#endif