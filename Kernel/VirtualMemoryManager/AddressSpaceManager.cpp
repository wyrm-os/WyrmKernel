#include <string.h>
#include <mathutil.h>
#include "../CPU/CPU.h"
#include "AddressSpaceManager.h"
#include "PagingUtility.h"
#include "../MemoryManager/PageAllocator.h"
#include "../Collections/List.h"
#include "../CPU/cpu.h"

AddressSpaceManager::AddressSpaceManager(PageAllocator & pa)
	: pageAllocator(pa) {
	pml4 = createTable();

	//Set up the pml4 with pdpts, this tables will be used by all mappings, kernel and user
	for (int i = 0; i < 512; i++)
		pml4[i].setPresent(true).setWritable(true).setUser(true)
			.setPageWriteThrough(false).setPageCacheDisable(false)
			.setAccessed(false).setPageSize(false).setExecuteDisable(false)
			.set4KiBPageAddress(createTable());

	PageTableEntry * pageDirectoryPointerTable = (PageTableEntry*)pml4[0].get4KiBPageAddress();
	char * page1GiB = (char*)0x0;

	//Identity map first 512 GiB (that should be all memory)
	for (int i = 0; i < 512; i++, page1GiB += oneGiB)
		pageDirectoryPointerTable[i].setPresent(true).setWritable(true)
			.setUser(false).setPageWriteThrough(false)
			.setPageCacheDisable(false).setAccessed(false).setPageSize(true)
			.set1GiBPageAddress(page1GiB);

	//Update CR3 with the new mapping
	writeCR3((uint64_t)pml4);
}

void AddressSpaceManager::swap(AddressSpace & out, AddressSpace & in) {
	justUnmap(out);
	justMap(in);
	invalidateAddresses(
		(void*)min((uint64_t)out.startAddress(), (uint64_t)in.startAddress()),
		(void*)max((uint64_t)out.endAddress(), (uint64_t)in.endAddress())
	);
}

void AddressSpaceManager::map(AddressSpace & space) {
	justMap(space);
	invalidateAddresses(space.startAddress(), space.endAddress());
}

void AddressSpaceManager::justMap(AddressSpace & space) {
	uint32_t pml4Index = PagingUtility::getPML4Entry(space.startAddress());
	uint32_t pdptIndex = PagingUtility::getPageDirectoryPointerTableEntry(space.startAddress());
	List<PageTableEntry>::Iterator i = space.begin();
	List<PageTableEntry>::Iterator end = space.end();

	for (; i != end; i++, pdptIndex++) {
		pml4Index += pdptIndex / 512;
		pdptIndex %= 512;

		PageTableEntry * pdpt = (PageTableEntry*)pml4[pml4Index].get4KiBPageAddress();
		pdpt[pdptIndex] = *i;
	}
}

void AddressSpaceManager::unmap(AddressSpace & space) {
	justUnmap(space);
	invalidateAddresses(space.startAddress(), space.endAddress());
}

void AddressSpaceManager::justUnmap(AddressSpace & space) {
	uint32_t pml4Index = PagingUtility::getPML4Entry(space.startAddress());
	uint32_t pdptIndex = PagingUtility::getPageDirectoryPointerTableEntry(space.startAddress());
	List<PageTableEntry>::Iterator i = space.begin();
	List<PageTableEntry>::Iterator end = space.end();

	for (; i != end; i++, pdptIndex++) {
		pml4Index += pdptIndex / 512;
		pdptIndex %= 512;

		PageTableEntry * pdpt = (PageTableEntry*)pml4[pml4Index].get4KiBPageAddress();
		pdpt[pdptIndex].setPresent(false);
	}
}

PageTableEntry * AddressSpaceManager::createTable() {
	PageTableEntry * table = (PageTableEntry*)pageAllocator.alloc(1);
	memset(table, 0, PageAllocator::PageSize);
	return table;
}

void AddressSpaceManager::invalidateAddresses(void * from, void * to) {
	for (char * address = (char*)from; address < to; address += PageAllocator::PageSize)
		invalidateVirtualAddress(address);
}
