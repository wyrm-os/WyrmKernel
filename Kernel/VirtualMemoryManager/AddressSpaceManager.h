#ifndef PAGING_STRUCTURE_MAPPER_H
#define PAGING_STRUCTURE_MAPPER_H

#include "PageTableEntry.h"
#include "AddressSpace.h"
#include "../MemoryManager/PageAllocator.h"

class AddressSpaceManager {
private:
	const uint64_t oneGiB = 1024 * 1024 * 1024;

	PageTableEntry * pml4;
	PageAllocator pageAllocator;

public:
	AddressSpaceManager(PageAllocator & pageAllocator);
	void swap(AddressSpace & out, AddressSpace & in);
	void map(AddressSpace & space);
	void unmap(AddressSpace & space);

private:
	PageTableEntry * createTable();
	void justMap(AddressSpace & space);
	void justUnmap(AddressSpace & space);
	void invalidateAddresses(void * from, void * to);
};

#endif
