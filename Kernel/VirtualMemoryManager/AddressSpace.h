#ifndef ADDRESS_SPACE_H
#define ADDRESS_SPACE_H

#include <stdint.h>
#include "PageTableEntry.h"
#include "../MemoryManager/PageAllocator.h"
#include "../Collections/List.h"

class AddressSpace {
private:
	void * virtualBase;
	List<PageTableEntry> pdptEntries;
	int pageCount;
	PageAllocator & pageAllocator;

public:
	AddressSpace(void * virtualBase, uint32_t pageCount, PageAllocator & pageAllocator);
	~AddressSpace();
	bool growUp(uint64_t pages);
	bool growDown(uint64_t pages);
	void * startAddress();
	void * endAddress();
	List<PageTableEntry>::Iterator begin();
	List<PageTableEntry>::Iterator end();

private:
	bool growUp();
	bool growDown();
	bool grow(void * virtualAddressToMap);
};

#endif