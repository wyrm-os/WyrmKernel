#include "AddressSpace.h"
#include "PagingUtility.h"
#include "../MemoryManager/PAgeAllocatorTransaction.h"

//TODO: implement 4MB pages
AddressSpace::AddressSpace(void * virtualBase, uint32_t pageCount, PageAllocator & pa) : pageAllocator(pa) {
	this->virtualBase = virtualBase;
	pageCount = 0;
	PageTableEntry pdpte;
	growUp(pageCount);
}

bool AddressSpace::growUp(uint64_t pages) {
	bool success = true;

	while(pages-- && success)
		success &= growUp();

	return success;
}

bool AddressSpace::growDown(uint64_t pages) {
	bool success = true;

	while(pages-- & success)
		success &= growDown();

	return success;
}

AddressSpace::~AddressSpace() {
	//TODO: Implement address space release. Probably will attach the pages to a process in charge of
	//blanking and releasing the pages.
}

bool AddressSpace::growUp() {
	return grow((char*)virtualBase + pageCount * PageAllocator::PageSize);
}

bool AddressSpace::growDown() {
	if (grow((char*)virtualBase - PageAllocator::PageSize)) {
		virtualBase = (char*)virtualBase - PageAllocator::PageSize;
		return true;
	}

	return false;
}

void * AddressSpace::startAddress() {
	return virtualBase;
}

void * AddressSpace::endAddress() {
	return (char*)virtualBase + pageCount * PageAllocator::PageSize;
}

List<PageTableEntry>::Iterator AddressSpace::begin() {
	return pdptEntries.begin();
}

List<PageTableEntry>::Iterator AddressSpace::end() {
	return pdptEntries.end();
}

bool AddressSpace::grow(void * virtualAddressToMap) {
	PageAllocatorTransaction<3> allocator(pageAllocator);
	uint32_t pdIndex = PagingUtility::getPageDirectoryEntry(virtualAddressToMap);
	uint32_t ptIndex = PagingUtility::getPageTableEntry(virtualAddressToMap);

	//New pdpt entry is needed
	if (PagingUtility::is1GiBAligned(virtualAddressToMap) || pdptEntries.count() == 0) {
		if (allocator.alloc(1))
			return allocator.failWithFalse();

		PageTableEntry * pageDirectory = PageTableEntry::clear((PageTableEntry*)allocator.get(1));

		PageTableEntry entry;
		entry.setPresent(true).setWritable(true).set4KiBPageAddress(pageDirectory);
		pdptEntries.pushBack(entry);
	}

	PageTableEntry * pageDirectory = (PageTableEntry*)pdptEntries.back().get4KiBPageAddress();

	//New pdt entry is needed
	if (PagingUtility::is2MiBAligned(virtualAddressToMap)) {
		if (allocator.alloc(1))
			return allocator.failWithFalse();

		PageTableEntry * pageTable = PageTableEntry::clear((PageTableEntry*)allocator.get(2));

		PageTableEntry entry;
		entry.setPresent(true).setWritable(true).set4KiBPageAddress(pageTable);

		pageDirectory[pdIndex] = entry;
	}

	//Map the physical page
	if (allocator.alloc(1))
		return allocator.failWithFalse();

	PageTableEntry * pageTable = (PageTableEntry*)pageDirectory[pdIndex].get4KiBPageAddress();
	pageTable[ptIndex].setPresent(true).setWritable(true).set4KiBPageAddress(allocator.get(3));
	pageCount++;

	return true;
}
