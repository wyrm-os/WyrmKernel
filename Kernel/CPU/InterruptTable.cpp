#include "cpu.h"
#include "InterruptTable.h"
#include "../Collections/BitArray.h"

InterruptTable::InterruptTable(void * idt, uint16_t offsetPIC1, uint16_t offsetPIC2) {
	RemapPIC(offsetPIC1, offsetPIC2);
	maskPIC1 = 0xFF;
	maskPIC2 = 0xFF;
	outb(PIC1Data, maskPIC1);
	outb(PIC2Data, maskPIC2);
	this->idt = (InterruptDescriptor*)idt;
}

InterruptTable & InterruptTable::setHandler(uint32_t index, InterruptHandler handler) {
	idt[index]
		.setHandler(handler)
		.setSegmentSelector(8)
		.setPresent(true)
		.setPrivilegeLevel(Ring0)
		.setStorageSegment(false)
		.setGateType(InterruptGate32);
	return *this;
}

InterruptTable & InterruptTable::setIRQMask(uint8_t index, bool value) {
	if (index < 8) {
		BitArray::set(&maskPIC1, index, value);
		outb(0x21, maskPIC1);
	} else if (index < 16) {
		BitArray::set(&maskPIC2, index - 8, value);
		outb(0xA1, maskPIC2);
	}

	return *this;
}

InterruptHandler InterruptTable::getHandler(uint32_t index) {
	return idt[index].getHandler();
}

bool InterruptTable::getIRQMask(uint8_t index) {
	return index < 8?
		BitArray::get(&maskPIC1, index):
		BitArray::get(&maskPIC2, index - 8);
}
 
void InterruptTable::RemapPIC(uint16_t offsetPIC1, uint16_t offsetPIC2)
{
	// Start the initialization sequence (in cascade mode)
	outb(PIC1Command, ICW1Init + ICW1ICW4);
	outb(PIC2Command, ICW1Init + ICW1ICW4);

	// ICW2: Master PIC vector offset
	outb(PIC1Data, offsetPIC1);                 

	// ICW2: Slave PIC vector offset
	outb(PIC2Data, offsetPIC2);                 

	// ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	outb(PIC1Data, 4);

	// ICW3: tell Slave PIC its cascade identity (0000 0010)
	outb(PIC2Data, 2);
 
	outb(PIC1Data, ICW48086);
	outb(PIC2Data, ICW48086);
}
