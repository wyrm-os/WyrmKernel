; Platform:					System V x86_64
; Return Value:				rax, rdx
; Parameter Registers:		rdi, rsi, rdx, rcx, r8, r9
; Additional Parameters:	stack (right to left)
; Stack Alignment:			16-byte at call
; Scratch Registers:		rax, rdi, rsi, rdx, rcx, r8, r9, r10, r11
; Preserved Registers:		rbx, rsp, rbp, r12, r13, r14, r15
; Call List:				rbp

global writeCR0
global readCR0
global writeCR1
global readCR1
global writeCR2
global readCR2
global writeCR3
global readCR3
global writeCR4
global readCR4
global writeMSR
global readMSR
global cpuid
global outport
global inport
global disableInterrupts
global enableInterrupts
global finishStartup
global outb
global outd
global outq
global inb
global ind
global inq
global int80
global invalidateVirtualAddress

extern switchUserToKernel
extern switchKernelToUser
extern getCurrentEntryPoint

; void writeCR0(uint64_t value)
writeCR0:
	mov cr0, rdi
	ret

; uint64_t readCR0()
readCR0:
	mov rax, cr0
	ret

; void writeCR2(uint64_t value)
writeCR2:
	mov cr2, rdi
	ret

; uint64_t readCR2()
readCR2:
	mov rax, cr2
	ret

; void writeCR3(uint64_t value)
writeCR3:
	mov cr3, rdi
	ret

; uint64_t readCR3()
readCR3:
	mov rax, cr3
	ret

; void writeCR4(uint64_t value)
writeCR4:
	mov cr4, rdi
	ret

; uint64_t readCR4()
readCR4:
	mov rax, cr4
	ret

; void writeMSR(uint32_t registryId, uint64_t value)
writeMSR:
	mov rcx, rdi
	mov rax, rsi	;low
	shl rax, 32
	shr rax, 32
	shr rdi, 32
	mov rdx, rsi	;high
	WRMSR
	ret

; void readMSR(uint32_t registryId, uint64_t * value)
reardMSR:
	mov rcx, rdi
	RDMSR
	mov [rsi], eax
	mov [rsi + 4], edx
	ret

; void cpuid(uint32_t eax, uint32_t * eax, uint32_t * ebx,  uint32_t * ecx, uint32_t * edx)
cpuid:
	push rbx		; Save rbx since it's a preserved register and we'll use it

	mov rax, rdi	; Set the operator
	mov r9, rdx		; Save rdx and rcx since cpuid overwrites them
	mov r10, rcx
	cpuid

	mov [rsi], eax
	mov [r9], ebx
	mov [r10], ecx
	mov [r8], edx

	pop rbx			; Restore rbx
	ret

; void outb(uint16_t port, uint8_t value)
outb:
	mov rdx, rdi
	mov rax, rsi
	out dx, al
	ret

; void outd(uint16_t port, uint16_t value)
outd:
	mov rdx, rdi
	mov rax, rsi
	out dx, ax
	ret

; void outq(uint16_t port, uint32_t value)
outq:
	mov rdx, rdi
	mov rax, rsi
	out dx, eax
	ret

; uint8_t inb(uint16_t port)
inb:
	mov rdx, rdi
	in al, dx
	ret

; uint16_t ind(uint16_t port)
ind:
	mov rbx, rdi
	in ax, dx
	ret

; uint32_t inq(uint16_t port)
inq:
	mov rbx, rdi
	in eax, dx
	ret

;void int80()
int80:
	int 80h
	ret

; void disableInterrupts()
disableInterrupts:
	cli
	ret

; void enableInterrupts()
enableInterrupts:
	sti
	ret

;void finishStartup()
finishStartup:
	; get the first process's RSP and load it
	call switchKernelToUser
	mov rsp, rax

	; launch the first process
	call getCurrentEntryPoint
	
	;this should probably be done by a kernel process
	sti		
	
	jmp rax

;void invalidateVirtualAddress(void * virtualAddress);
invalidateVirtualAddress:
	mov rax, rdi
	invlpg [rax]
	ret
