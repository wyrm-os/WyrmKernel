#ifndef IRQ_HANDLERS_H
#define IRQ_HANDLERS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void irq0Handler();
void int80Handler();

#ifdef __cplusplus
}
#endif

#endif