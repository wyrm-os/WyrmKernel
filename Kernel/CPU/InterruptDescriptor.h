#ifndef INTERRUPT_DESCRIPTOR
#define INTERRUPT_DESCRIPTOR

#include <stdint.h>
#include "../Log/Log.h"

typedef void (*InterruptHandler)();

typedef enum  {
	Ring0 = 0,
	Ring1 = 1,
	Ring2 = 2,
	Ring3 = 3
} PrivilegeLevel;

typedef enum {
	TaskGate 		= 0x5,
	InterruptGate	= 0x6,
	TrapGate 		= 0x7,
	InterruptGate32	= 0xE,
	TrapGate32		= 0xF
} GateType;

struct InterruptDescriptor {
	uint16_t lowBits;			//Offset low bits (0..15)
	uint16_t segmentSelector;	//Selector (Code segment selector)
	uint8_t reservedField0;		//Zero
	uint8_t typeAndAttributes;	//Type and Attributes
	uint16_t middleBits;		//Offset middle bits (16..31)
	uint32_t highBits;			//Offset high bits (32..63)
	uint32_t reservedField1;	//Zero

	InterruptDescriptor();

	InterruptDescriptor & setHandler(InterruptHandler handler);
	InterruptDescriptor & setSegmentSelector(uint16_t segmentSelector);
	InterruptDescriptor & setPresent(bool value);
	InterruptDescriptor & setPrivilegeLevel(PrivilegeLevel value);
	InterruptDescriptor & setStorageSegment(bool value);
	InterruptDescriptor & setGateType(GateType value);

	InterruptHandler getHandler();
	uint16_t getSegmentSelector();
	bool getPresent();
	PrivilegeLevel getPrivilegeLevel();
	bool getStorageSegment();
	GateType getGateType();
	void log(Log & log);
};

#endif