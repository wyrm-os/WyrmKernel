global irq0Handler
global int80Handler

extern switchUserToKernel
extern switchKernelToUser
extern syscallHandler

%macro	pushState 0
	push rax
	push rbx
	push rcx
	push rdx
	push rbp
	push rdi
	push rsi
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15
	push fs
	push gs
%endmacro

%macro	popState 0
	pop gs
	pop fs
	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rsi
	pop rdi
	pop rbp
	pop rdx
	pop rcx
	pop rbx
	pop rax
%endmacro

; void irq0Handler()
irq0Handler:
	pushState

	; save current process's RSP
	mov rdi, rsp

	; enter kernel context by setting current process's kernel-RSP
	call switchUserToKernel
	;xchg bx, bx

	mov rsp, rax

	; schedule, get new process's RSP and load it
	call switchKernelToUser
	;xchg bx, bx

	mov rsp, rax

	; Send end of interrupt
	mov al, 0x20
	out 0x20, al

	popState
	iretq

; void int0Handler()
int80Handler:
	pushState

;	call syscallHandler

	popState
	iretq

