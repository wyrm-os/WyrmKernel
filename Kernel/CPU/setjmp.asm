global setjmp
global longjmp

setjmp:
	mov rcx, rdi
	mov rdx, [rsp+0]
	mov [rcx+0], rdx
	mov [rcx+8], rbx
	mov [rcx+16], rsp
	mov [rcx+24], rbp
	mov [rcx+32], r12
	mov [rcx+40], r13
	mov [rcx+48], r14
	mov [rcx+56], r15
	fnstcw [rcx+64]
	stmxcsr [rcx+68]
	xor rax, rax
	ret

longjmp:
	mov rdx, rdi
	stmxcsr [rsp-4]
	mov eax, [rdx+68]
	and eax, 0xFFFFFFC0
	mov edi, [rsp-4]
	and edi, 0x3F
	xor edi, eax
	mov [rsp-4], edi
	ldmxcsr [rsp-4]
	mov rax, rsi
	mov rcx, [rdx+0]
	mov rbx, [rdx+8]
	mov rsp, [rdx+16]
	mov rbp, [rdx+24]
	mov r12, [rdx+32]
	mov r13, [rdx+40]
	mov r14, [rdx+48]
	mov r15, [rdx+56]
	fldcw [rdx+64]
	test rax, rax
	jnz	a
	inc rax
a:	mov [rsp+0], rcx
	ret
