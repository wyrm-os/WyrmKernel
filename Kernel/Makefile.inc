GCC=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-gcc
GPP=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-g++
LD=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-ld
AR=$(WYRM_CROSS_COMPILER_PATH)/x86_64-elf-ar
ASM=nasm

COMMONFLAGS=-I$(WYRM_PATH)/StandardLibrary -mno-mmx -mno-sse -mno-sse2 -fno-builtin-malloc -fno-builtin-free -fno-builtin-realloc -mno-red-zone -Wall -ffreestanding -nostdlib -fno-common
GPPFLAGS=$(COMMONFLAGS) -I$(WYRM_PATH)/Collections -fno-exceptions -fno-rtti -fno-asynchronous-unwind-tables -std=gnu++0x
GCCFLAGS=$(COMMONFLAGS) -std=c99
HOSTGPPFLAGS=-I$(WYRM_PATH)/Collections -std=gnu++0x
ARFLAGS=rvs
ASMFLAGS=-felf64
