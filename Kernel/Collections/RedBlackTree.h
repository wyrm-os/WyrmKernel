#ifndef REDBLACKTREE_H
#define REDBLACKTREE_H

#include <stdint.h>
#include <stddef.h>
#include "RedBlackTreeNode.h"
#include "RedBlackTreeNodeImpl.h"

template<typename T>
class RedBlackTree {
public:
	RedBlackTree();
	static RedBlackTreeNode<T> * createNodeFromMemoryChunk(void * chunk, T & element);
	static uint32_t getNodeSize();

	void insert(RedBlackTreeNode<T> * node);
	void remove(RedBlackTreeNode<T> * node);
	RedBlackTreeNode<T> * search(T & element);
	RedBlackTreeNode<T> * getRoot();

private:
	RedBlackTreeNodeImpl<T> * root;	

	void insertCase1(RedBlackTreeNodeImpl<T> * node);
	void insertCase2(RedBlackTreeNodeImpl<T> * node);
	void insertCase3(RedBlackTreeNodeImpl<T> * node);
	void insertCase4(RedBlackTreeNodeImpl<T> * node);
	void insertCase5(RedBlackTreeNodeImpl<T> * node);

	void deleteCase1(RedBlackTreeNodeImpl<T> * node);
	void deleteCase2(RedBlackTreeNodeImpl<T> * node);
	void deleteCase3(RedBlackTreeNodeImpl<T> * node);
	void deleteCase4(RedBlackTreeNodeImpl<T> * node);
	void deleteCase5(RedBlackTreeNodeImpl<T> * node);
	void deleteCase6(RedBlackTreeNodeImpl<T> * node);

	void rotateLeft(RedBlackTreeNodeImpl<T> * node);
	void rotateRight(RedBlackTreeNodeImpl<T> * node);

	void changeParent(RedBlackTreeNodeImpl<T> * oldChild, RedBlackTreeNodeImpl<T> * newChild);
	RedBlackTreeNodeImpl<T> ** getParentReference(RedBlackTreeNodeImpl<T> * node);
	void swapNodes(RedBlackTreeNodeImpl<T> * node1, RedBlackTreeNodeImpl<T> * node2);
	void deleteOneChild(RedBlackTreeNodeImpl<T> * node, RedBlackTreeNodeImpl<T> * child);
	static void swapNodeReferences(RedBlackTreeNodeImpl<T> ** node1, RedBlackTreeNodeImpl<T> ** node2);
	static void relinkChild(RedBlackTreeNodeImpl<T> * node, RedBlackTreeNodeImpl<T> * child);
};

template<typename T>
RedBlackTree<T>::RedBlackTree() {
	root = (RedBlackTreeNodeImpl<T>*)NULL;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTree<T>::createNodeFromMemoryChunk(void * chunk, T & element) {
	return new(chunk) RedBlackTreeNodeImpl<T>(element);
}

template<typename T>
void RedBlackTree<T>::insert(RedBlackTreeNode<T> * node) {
	RedBlackTreeNodeImpl<T> * nodeImpl = (RedBlackTreeNodeImpl<T> *)node;
	RedBlackTreeNodeImpl<T> * parent = NULL;
	RedBlackTreeNodeImpl<T> ** current = &root;

	while (*current != NULL) {
		parent = *current;

		if ((*current)->element < nodeImpl->element)
			current = &((*current)->right);
		else
			current = &((*current)->left);
	}

	*current = nodeImpl;
	nodeImpl->color = RBTRed;
	nodeImpl->parent = parent;

	insertCase1(nodeImpl);
}

template<typename T>
void RedBlackTree<T>::remove(RedBlackTreeNode<T> * node) {
	//Remove must actually remove the node passed as a parameter, not just it's value
	RedBlackTreeNodeImpl<T> * nodeImpl = (RedBlackTreeNodeImpl<T> *)node;
	bool nodeDeleted = false;
	T tmp = nodeImpl->element;

	while(!nodeDeleted) {
		if (node == NULL)
			nodeDeleted = true;
		else if (nodeImpl->right == NULL && nodeImpl->left == NULL) {
			if (nodeImpl->color == RBTBlack)
				deleteCase1(nodeImpl);	//This is valid, the node behaves as a leaf and will continue to be a leaf after the operations

			changeParent(nodeImpl, NULL);
			nodeImpl->parent = NULL;
			nodeDeleted = true;
		} else if (nodeImpl->right == NULL) {
			changeParent(nodeImpl, nodeImpl->left);
			deleteOneChild(nodeImpl, nodeImpl->left);
			nodeImpl->left = NULL;
			nodeImpl->parent = NULL;
			nodeDeleted = true;
		} else if (nodeImpl->left == NULL) {
			changeParent(nodeImpl, nodeImpl->right);
			deleteOneChild(nodeImpl, nodeImpl->right);
			nodeImpl->right = NULL;
			nodeImpl->parent = NULL;
			nodeDeleted = true;
		} else {
			RedBlackTreeNodeImpl<T> * successor = (RedBlackTreeNodeImpl<T> *)nodeImpl->inOrderNext();

			//Do not just copy the value from the successor to the node as in a regular BST delete,
			//but replace the successor with the node and recursively delete
			nodeImpl->element = successor->element;
			swapNodes(nodeImpl, successor);
		}
	}

	//Restore original element
	nodeImpl->element = tmp;
}

template<typename T>
void RedBlackTree<T>::deleteOneChild(RedBlackTreeNodeImpl<T> * node, RedBlackTreeNodeImpl<T> * child) {
	if (node->color == RBTBlack) {
		if (child->color == RBTRed)
			child->color = RBTBlack;
		else
			deleteCase1(child);
	}
}

template<typename T>
void RedBlackTree<T>::deleteCase1(RedBlackTreeNodeImpl<T> * node) {
	if (node->parent != NULL)
		deleteCase2(node);
}

template<typename T>
void RedBlackTree<T>::deleteCase2(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * sibling = (RedBlackTreeNodeImpl<T> *)node->getSibling();

	if (sibling != NULL && sibling->color == RBTRed) {
		node->parent->color = RBTRed;
		sibling->color = RBTBlack;

		if (node == node->parent->left)
			rotateLeft(node->parent);
		else
			rotateRight(sibling);
	}

	deleteCase3(node);
}

template<typename T>
void RedBlackTree<T>::deleteCase3(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * sibling = (RedBlackTreeNodeImpl<T> *)node->getSibling();

	if (node->parent->color == RBTBlack &&
		sibling->color == RBTBlack &&
		(sibling->left == NULL || sibling->left->color == RBTBlack) &&
		(sibling->right == NULL || sibling->right->color == RBTBlack)) {
		sibling->color = RBTRed;
		deleteCase1(node->parent);
	} else {
		deleteCase4(node);
	}
}

template<typename T>
void RedBlackTree<T>::deleteCase4(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * sibling = (RedBlackTreeNodeImpl<T> *)node->getSibling();

	if (node->parent->color == RBTRed &&
		sibling->color == RBTBlack &&
		(sibling->left == NULL || sibling->left->color == RBTBlack) &&
		(sibling->right == NULL || sibling->right->color == RBTBlack)) {
		sibling->color = RBTRed;
		node->parent->color = RBTBlack;
	} else {
		deleteCase5(node);
	}
}

template<typename T>
void RedBlackTree<T>::deleteCase5(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * sibling = (RedBlackTreeNodeImpl<T> *)node->getSibling();

	if (sibling->color == RBTBlack) {
		if (node == node->parent->left &&
			(sibling->right == NULL || sibling->right->color == RBTBlack) &&
			sibling->left != NULL && sibling->left->color == RBTRed) {
			sibling->color = RBTRed;
			sibling->left->color = RBTBlack;
			rotateRight(sibling->left);
		} else if (node == node->parent->right &&
			(sibling->left == NULL || sibling->left->color == RBTBlack) &&
			sibling->right != NULL && sibling->right->color == RBTRed) {
			sibling->color = RBTRed;
			sibling->right->color = RBTBlack;
			rotateLeft(sibling);
		}
	}

	deleteCase6(node);
}

template<typename T>
void RedBlackTree<T>::deleteCase6(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * sibling = (RedBlackTreeNodeImpl<T> *)node->getSibling();

	sibling->color = node->parent->color;
	node->parent->color = RBTBlack;

	if (node == node->parent->left) {
		sibling->right->color = RBTBlack;
		rotateLeft(node->parent);
	} else {
		sibling->left->color = RBTBlack;
		rotateRight(sibling);
	}
}

template<typename T>
void RedBlackTree<T>::swapNodes(RedBlackTreeNodeImpl<T> * node1, RedBlackTreeNodeImpl<T> * node2) {
	//Swap childs
	swapNodeReferences(&node1->left, &node2->left);
	swapNodeReferences(&node1->right, &node2->right);

	//Update child's parent references
	relinkChild(node1, node1->left);
	relinkChild(node1, node1->right);
	relinkChild(node2, node2->left);
	relinkChild(node2, node2->right);
	
	//Swap parent's references
	swapNodeReferences(getParentReference(node1), getParentReference(node2));

	//Swap parents
	swapNodeReferences(&node1->parent, &node2->parent);

	//Swap colors
	RedBlackTreeColor temp = node1->color;
	node1->color = node2->color;
	node2->color = temp;
}

template<typename T>
void RedBlackTree<T>::relinkChild(RedBlackTreeNodeImpl<T> * node, RedBlackTreeNodeImpl<T> * child) {
	if (child)
		child->parent = node;
}

template<typename T>
RedBlackTreeNodeImpl<T> ** RedBlackTree<T>::getParentReference(RedBlackTreeNodeImpl<T> * node) {
	return node->parent == NULL?
		&this->root :
		node->parent->left == node?
			&node->parent->left :
			&node->parent->right;
}

template<typename T>
void RedBlackTree<T>::swapNodeReferences(RedBlackTreeNodeImpl<T> ** node1, RedBlackTreeNodeImpl<T> ** node2) {
	RedBlackTreeNodeImpl<T> * temp = *node1;
	*node1 = *node2;
	*node2 = temp;
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTree<T>::search(T & element) {
	return root == NULL? NULL : root->search(element);
}

template<typename T>
RedBlackTreeNode<T> * RedBlackTree<T>::getRoot() {
	return root;
}

template<typename T>
uint32_t RedBlackTree<T>::getNodeSize() {
	return sizeof(RedBlackTreeNodeImpl<T>);
}

template<typename T>
void  RedBlackTree<T>::insertCase1(RedBlackTreeNodeImpl<T> * node) {
	if (node->parent == NULL)
 		node->color = RBTBlack;
	else
		insertCase2(node);
}

template<typename T>
void RedBlackTree<T>::insertCase2(RedBlackTreeNodeImpl<T> * node) {
	if (node->parent->color == RBTBlack)
		return;
	else
		insertCase3(node);
}

template<typename T>
void RedBlackTree<T>::insertCase3(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * uncle = (RedBlackTreeNodeImpl<T>*)node->getUncle();
	RedBlackTreeNodeImpl<T> * grandparent;

	if ((uncle != NULL) && (uncle->color == RBTRed)) {
		node->parent->color = RBTBlack;
		uncle->color = RBTBlack;
		grandparent = (RedBlackTreeNodeImpl<T>*)node->getGrandparent();
		grandparent->color = RBTRed;
		insertCase1(grandparent);
	} else {
		insertCase4(node);
	}
}

template<typename T>
void RedBlackTree<T>::insertCase4(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * grandparent = (RedBlackTreeNodeImpl<T>*)node->getGrandparent();

	if ((node == node->parent->right) && (node->parent == grandparent->left)) {
		rotateLeft(node->parent);
		node = node->left;
	} else if ((node == node->parent->left) && (node->parent == grandparent->right)) {
		rotateRight(node);
		node = node->right; 
	}

	insertCase5(node);
}

template<typename T>
void RedBlackTree<T>::insertCase5(RedBlackTreeNodeImpl<T> * node) {
	RedBlackTreeNodeImpl<T> * grandparent = (RedBlackTreeNodeImpl<T>*)node->getGrandparent();

	node->parent->color = RBTBlack;
	grandparent->color = RBTRed;

	if (node == node->parent->left)
		rotateRight(node->parent);
	else
		rotateLeft(grandparent);
}

template<typename T>
void RedBlackTree<T>::rotateLeft(RedBlackTreeNodeImpl<T> * pivot) {
	RedBlackTreeNodeImpl<T> * right = pivot->right;

	changeParent(pivot, right);

	pivot->right = right->left;
	if (right->left)
		right->left->parent = pivot;

	right->left = pivot;
	pivot->parent = right;
}

template<typename T>
void RedBlackTree<T>::rotateRight(RedBlackTreeNodeImpl<T> * pivot) {
	RedBlackTreeNodeImpl<T> * root = pivot->parent;

	changeParent(root, pivot);

	root->left = pivot->right;
	if (pivot->right)
		pivot->right->parent = root;

	pivot->right = root;
	root->parent = pivot;
}

template<typename T>
void RedBlackTree<T>::changeParent(RedBlackTreeNodeImpl<T> * oldChild, RedBlackTreeNodeImpl<T> * newChild) {
	RedBlackTreeNodeImpl<T> * parent = oldChild->parent;

	if (newChild)
		newChild->parent = parent;

	if (parent) {
		if (parent->left == oldChild) {
			parent->left = newChild;
		} else {
			parent->right = newChild;
		}
	} else {
		this->root = newChild;
	}
}

#endif