#ifndef BITARRAY_H
#define BITARRAY_H

#include <stdint.h>

class BitArray {
private:
	uint8_t * array;
	uint32_t size;

public:
	BitArray(uint8_t * mem, uint32_t sizeInBytes);
	bool get(uint32_t index);
	void set(uint32_t index, bool value);

	static void initialize(uint8_t * start, uint64_t size);
	static bool get(uint8_t * mem, uint32_t index);
	static void set(uint8_t * mem, uint32_t index, bool value);
};

#endif