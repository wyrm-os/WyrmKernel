#include <stdint.h>
#include "BitArray.h"

BitArray::BitArray(uint8_t * mem, uint32_t sizeInBytes) {
	array = mem;
	size = sizeInBytes * 8;
}

void BitArray::initialize(uint8_t * start, uint64_t size) {
	for (uint64_t i = 0; i < size; i++)
		start[i] = 0;
}

bool BitArray::get(uint32_t index) {
	if (index >= size)
		return false;
	return BitArray::get(array, index);
}

void BitArray::set(uint32_t index, bool value) {
	if (index >= size)
		return;
	BitArray::set(array, index, value);
}

bool BitArray::get(uint8_t * mem, uint32_t index) {
	uint32_t byte = index / 8;
	uint32_t bit = index % 8;

	return (1 << bit) & mem[byte];
}

void BitArray::set(uint8_t * mem, uint32_t index, bool value) {
	uint32_t byte = index / 8;
	uint32_t bit = index % 8;

	if (value)
		mem[byte] |= (1 << bit);
	else
		mem[byte] &= ~(1 << bit);
}
