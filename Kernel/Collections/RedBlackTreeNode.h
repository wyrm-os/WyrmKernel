#ifndef REDBLACKTREENODE_H
#define REDBLACKTREENODE_H

enum RedBlackTreeColor { RBTBlack, RBTRed };

template<typename T>
class RedBlackTreeNode {
public:
	virtual RedBlackTreeNode<T> * getParent() = 0;
	virtual RedBlackTreeNode<T> * getLeftChild() = 0;
	virtual RedBlackTreeNode<T> * getRightChild() = 0;
	virtual RedBlackTreeNode<T> * getGrandparent() = 0;
	virtual RedBlackTreeNode<T> * getUncle() = 0;
	virtual RedBlackTreeNode<T> * inOrderPrev() = 0;
	virtual RedBlackTreeNode<T> * inOrderNext() = 0;
	virtual RedBlackTreeNode<T> * min() = 0;
	virtual RedBlackTreeNode<T> * min(T & cap) = 0;
	virtual RedBlackTreeNode<T> * max() = 0;
	virtual RedBlackTreeNode<T> * max(T & cap) = 0;
	virtual RedBlackTreeNode<T> * search(T & toSearch) = 0;
	virtual RedBlackTreeColor getColor() = 0;
	virtual T & getElement() = 0;
};

#endif