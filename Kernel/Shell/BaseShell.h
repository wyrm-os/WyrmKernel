#ifndef BASE_SHELL_H
#define BASE_SHELL_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>
#include "Command.h"
#include "HelpCommand.h"

using namespace std;

class BaseShell {
private:
	bool running;
	vector<Command*> * commands;
	void parseCommand(vector<Command*> * commands, HelpCommand * help);

protected:
	virtual string getShellName() = 0;
	virtual void beforeCommandExecution();
	virtual void afterCommandExecution();

public:
	void run(vector<Command*> * commands);
	bool quit();

private:
	string getCommandHelp(Command * command);
};

#endif
