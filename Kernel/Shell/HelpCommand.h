#ifndef HELP_COMMAND_H
#define HELP_COMMAND_H

#include <vector>
#include "BaseShell.h"
#include "Command.h"

using namespace std;

class HelpCommand : public Command {
private:
	vector<Command*> * commands;

public:
	HelpCommand(vector<Command*> * commands);

	string getName();
	string getCommand();
	string getHelp();
	void execute(CommandArguments & arguments);
	string getCommandHelp(Command * command);
};

#endif