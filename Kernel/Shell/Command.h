#ifndef COMMAND_H
#define COMMAND_H

#include <string>
#include "CommandArguments.h"

using namespace std;

class Command {
public:
	string name;
	string command;
	string help;

public:
	Command(string name, string command, string help);
	string getName();
	string getCommand();
	string getHelp();
	virtual void execute(CommandArguments & arguments) = 0;
};

#endif
