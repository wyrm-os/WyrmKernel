#include "Command.h"

Command::Command(string name, string command, string help) {
	this->name = name;
	this->command = command;
	this->help = help;
}

string Command::getName() {
	return name;
}

string Command::getCommand() {
	return command;
}

string Command::getHelp() {
	return help;
}
