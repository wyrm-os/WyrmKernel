#include <stdint.h>
#include <new>
#include "WyrmLog.h"

#define ATTRIBUTE		7
#define HMASK			0xFFFFFFFF00000000
#define LMASK			0x00000000FFFFFFFF

WyrmLog::WyrmLog() {
	void * videoRaw = (void*)Video;
	video = new (videoRaw) VideoCell[Columns * Lines];
	xPos = 0;
	yPos = 0;
	color = 0;
}

Log& WyrmLog::newline() {
	xPos = 0;

	if (yPos + 1 >= Lines) {
		scroll();
		return *this;
	}

	yPos++;
	return *this;
}

Log& WyrmLog::tab() {
	xPos += xPos % 4;

	if (xPos >= Columns)
		xPos = 0;

	return *this;
}

Log& WyrmLog::chr(int c) {
	if (c == '\n' || c == '\r') {
		newline();
		return *this;
	}

	if (c == '\t') {
		tab();
		return *this;
	}

	int pos = xPos + yPos * Columns;
	video[pos].character = c & 0xFF;
	video[pos].attribute = ATTRIBUTE;

	xPos++;

	if (xPos >= Columns) {
		newline();
	}

	return *this;
}

Log& WyrmLog::clear() {
	for (int i = 0; i < Columns * Lines; i++) {
		chr(' ');
	}

	xPos = yPos = 0;

	return *this;
}

void WyrmLog::scroll() {
	int r = Columns;
	int w = 0;
	int len = Columns * Lines;

	for (; r < len; r++, w++)
		video[w] = video[r];

	for (; w < len; w++) {
		video[w].character = ' ';
		video[w].attribute = ATTRIBUTE;
	}
}
