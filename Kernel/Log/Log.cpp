#include <stdint.h>
#include "Log.h"
#include "md5.h"

#define ATTRIBUTE		7
#define HMASK			0xFFFFFFFF00000000
#define LMASK			0x00000000FFFFFFFF

Log& Log::str(const char * string) {	
	while(*string)
		chr(*string++);

	return *this;
}

Log& Log::bits8(uint8_t value) {
	printBase(value, 8, 2);
	return *this;
}

Log& Log::bits16(uint16_t value) {
	printBase(value, 16, 2);
	return *this;
}

Log& Log::bits32(uint32_t value) {
	printBase(value, 32, 2);
	return *this;
}

Log& Log::bits64(uint64_t value) {
	printBase((value & HMASK) >> 32, 32, 2);
	printBase(value & LMASK, 32, 2);
	return *this;
}

Log& Log::addr(void * value) {
	str("0x");
	printBase(((uint64_t)value & HMASK) >> 32, 8, 16);
	printBase((uint64_t)value & LMASK, 8, 16);
	return *this;
}

Log& Log::laddr(void * value) {
	str("0x");
	printBase((uint64_t)value & LMASK, 8, 16);
	return *this;
}

Log& Log::haddr(void * value) {
	str("0x");
	printBase(((uint64_t)value & HMASK) >> 32, 8, 16);
	return *this;
}

Log& Log::count(uint32_t value) {
	printBase(value, 0, 10);
	return *this;
}

Log& Log::dec(int32_t value) {
	if (value < 0) {
		chr('-');
		value = -value;
	}

	printBase(value, 0, 10);
	return *this;
}

Log& Log::hex(uint64_t value, int digits) {
	if (digits > 8)
		printBase((value & HMASK) >> 32, digits - 8, 16);
	printBase(value & LMASK, digits > 8? 8 : digits, 16);
	return *this;
}

Log& Log::sizeb(uint64_t value) {
	//TODO Support 64 bit sizes
	printBase(value, 0, 10);
	str(" b");
	return *this;
}

Log& Log::sizeKb(uint64_t value) {
	//TODO Support 64 bit sizes
	//TODO Support decimals
	printBase(value / 1024, 0, 10);
	str(" Kb");
	return *this;
}

Log& Log::sizeMb(uint64_t value) {
	//TODO Support 64 bit sizes
	//TODO Support decimals
	printBase(value / (1024 * 1024), 0, 10);
	str(" Mb");
	return *this;
}

Log& Log::md5sum(void * address, uint64_t length) {
	unsigned char md5sum[16];

	MD5_CTX ctx;
	MD5_Init(&ctx);
	MD5_Update(&ctx, address, length);
	MD5_Final(md5sum, &ctx);

	for (int i=0; i < 16; i++)
		hex(md5sum[i], 2);

	return *this;
}

void Log::printBase(uint32_t value, int completeTo, int base) {
    int digits = uintToBase(value, buf, base);

    while (completeTo-- > digits)
    	chr('0');

    str(buf);
}

int Log::uintToBase(uint32_t value, char * buf, int base) {
	char *p = buf;
	char *p1, *p2;
	int digits = 0;

	do {
		int remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	} while (value /= base);

	// Terminate BUF.
	*p = 0;

	// Reverse BUF.
	p1 = buf;
	p2 = p - 1;
	while (p1 < p2) {
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}

	return digits;
}
