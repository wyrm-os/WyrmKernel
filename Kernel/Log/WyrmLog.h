#ifndef WYRMLOG_H
#define WYRMLOG_H

#include <stdint.h>
#include "Log.h"

class WyrmLog : public Log {
private:
	static const int Columns = 80;
	static const int Lines = 25;
	static const int Video = 0xB8000;

	VideoCell * video;
	int xPos;
	int yPos;
	char color;

public:
	WyrmLog();
	Log& newline();
	Log& tab();
	Log& chr(int c);
	Log& clear();
	//TODO Tab character

private:
	void scroll();
};

#endif
