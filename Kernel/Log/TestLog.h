#ifndef TESTLOG_H
#define TESTLOG_H

#include <stdint.h>
#include "Log.h"

class TestLog : public Log {
public:
	Log& newline();
	Log& chr(int c);
	Log& clear();
	Log& tab();
};

#endif
