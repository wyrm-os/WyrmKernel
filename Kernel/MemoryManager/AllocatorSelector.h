#ifndef ALLOCATORSELECTOR_H
#define ALLOCATORSELECTOR_H

#include "PageAllocator.h"
#include "ChunkAllocator.h"
#include "../Log/Log.h"
#include "../StandardLibrary/mathutil.h"

class AllocatorSelector {
private:
	static AllocatorSelector * instance;

	ChunkAllocator * allocators;
	PageAllocator & pageAllocator;
	Log & log;

public:
	AllocatorSelector(PageAllocator & pageAllocator, Log & log);

	static inline void * newImpl(uint64_t size) {
		return isSizeBigChunk(size)?
			instance->pageAllocator.alloc(upperDivide(size, PageAllocator::PageSize)):
			instance->getAllocator(size).alloc();
	}

	static inline void deleteImpl(void * toDelete) {
		if (isAddressBigChunk(toDelete))
			instance->pageAllocator.dealloc(toDelete);
		else
			instance->getAllocator(toDelete).dealloc(toDelete);
	}
private:
	static inline bool isAddressBigChunk(void * address) { return ((uint64_t)address & ~PageAllocator::PageMask) == 0; }
	static inline bool isSizeBigChunk(uint64_t size) { return size >= PageAllocator::PageSize; }

	inline ChunkAllocator & getAllocator(uint64_t size) {
		return allocators[size];
	}

	inline ChunkAllocator & getAllocator(void * allocated)  {
		return ChunkAllocator::getAllocatorFromPointer(allocated);
	}
};

#endif