#include <new>
#include "AllocatorSelector.h"

void * operator new(size_t size) {
	return AllocatorSelector::newImpl(size);
}

void operator delete(void * toDelete) {
	AllocatorSelector::deleteImpl(toDelete);
}
