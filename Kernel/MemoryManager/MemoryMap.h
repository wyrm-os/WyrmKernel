#ifndef MEMORYMAP_H
#define MEMORYMAP_H

#include "../Log/log.h"

struct MemoryMapEntry;

class MemoryMap {
public:
	//Assumptions:
	//	-First entry is always at 0x100000
	//	-Kernel is located at 0x100000
	//	-Stack is located after the kernel 
	MemoryMap(Log & log, void * kernelStack);
	MemoryMapEntry & operator[](uint32_t index);
	uint32_t count();
	void * startAddress();
	uint64_t totalMemory();
private:
	static MemoryMapEntry * originalMemoryMap;
	Log & log;
	uint32_t entryCount;

	static bool isBlank(MemoryMapEntry * entry);
	static bool isValid(MemoryMapEntry * entry);
	static int32_t memoryMapEntryComparer(MemoryMapEntry * e1, MemoryMapEntry * e2);
};

struct MemoryMapEntry {
	void * address;
	uint64_t size;
	uint32_t type;
	uint32_t extendedAttributes;
};

#endif