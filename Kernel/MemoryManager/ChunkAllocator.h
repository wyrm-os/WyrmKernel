#ifndef CHUNKALLOCATOR_H
#define CHUNKALLOCATOR_H

#include "PageAllocator.h"
#include "Chunk.h"
#include "../Log/Log.h"
#include "../Collections/RedBlackTree.h"

class ChunkAllocator {
private:
	static const uint32_t MinObjectsPerBlock = 8;

	RedBlackTree<Chunk> freeChunks; 
	PageAllocator & pageAllocator;
	uint32_t treeNodeSize;
	uint32_t userChunkSize;
	uint32_t pagesPerBlock;
	uint32_t objectsPerBlock;
	void * memoryStart;
	Log & log;

public:	
	ChunkAllocator(PageAllocator & pageAllocator, uint32_t chunkSize, Log & log);
	uint64_t getChunkSize();
	void * alloc();
	void dealloc(void * p);
	RedBlackTree<Chunk> & getFreeChunksTree();

	static ChunkAllocator & getAllocatorFromPointer(void * allocated);

private:
	static void * getMemoryChunkFromNode(RedBlackTreeNode<Chunk> * node);
	static RedBlackTreeNode<Chunk> * getNodeFromChunk(void * chunk);
	
	void * allocateChunk(RedBlackTreeNode<Chunk> * node, bool & removeNode);
	RedBlackTreeNode<Chunk> * createNode();
	bool canMerge(Chunk & chunkA, Chunk & chunkB);
	void merge(Chunk & chunkA, Chunk & chunkB);
	void logNode(Log & log, const char * name, RedBlackTreeNode<Chunk> * node);
};

#endif