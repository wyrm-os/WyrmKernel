#include <new>
#include <stdint.h>
#include "Chunk.h"

Chunk::Chunk(uint32_t objectCount, ChunkAllocator * a) {
 	this->allocator = a;
 	this->objectCount = objectCount;
}

bool Chunk::operator<(Chunk & chunk) {
	return this < &chunk;
}

bool Chunk::operator>(Chunk & chunk) {
	return this > &chunk;
}

bool Chunk::operator<=(Chunk & chunk) {
	return this <= &chunk;
}

bool Chunk::operator>=(Chunk & chunk) {
	return this >= &chunk;
}

bool Chunk::operator==(Chunk & chunk) {
	return this == &chunk;
}

bool Chunk::operator!=(Chunk & chunk) {
	return this != &chunk;
}
