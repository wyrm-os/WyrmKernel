#include <stdint.h>
#include <BitArray.h>
#include "../StandardLibrary/mathutil.h"
#include "PageAllocator.h"
#include "../Log/Log.h"

//Each PageAllocator object represents a pow 2 sized section of the memory,
//each allocator has a reference to the next one, alloc and free methods look
//for the proper section before 

//The alloc and free algorithms represent memory as a tree in a bit array.
//Each node in the tree is represented by two bits. The possible states are:

//	00 Free
//	01 Partial
//	10 Complete
//	11 Requested

//bit 0: partial/requested bit
//bit 1: complete bit

PageAllocator::PageAllocator(void * start, uint64_t size, Log & l) : log(l) {
	uint64_t pageCount = size / PageSize;
	uint32_t tableSizeInBits = NodeSize;

	managedPageCount = 1;
	totalSize = size;

	//Grow managedPageCount until we reach the lowest power of 2
	//count of pages greater than pageCount 
	while (managedPageCount < pageCount) {
		managedPageCount *= 2;
		tableSizeInBits = tableSizeInBits * 2 + NodeSize;
	}

	//Initialize the buddyTable
	buddyTable = (uint8_t*)start;
	tableSize = upperDivide(tableSizeInBits, 8);
	BitArray::initialize(buddyTable, tableSize);
	this->start = start;

	//Allocate the buddyTable so it doesn't get overwritten
	uint32_t tableSizeInPages = upperDivide(tableSize, PageSize);
	alloc(start, tableSizeInPages);

	//Mark as free the nodes in the tree that refer to memory outside the range
	//this nodes will never be freed
	blockSection((uint8_t*)start + size, (uint8_t*)0xFFFFFFFFFFFFF000);

	//Log
	log.str("Page Allocator:").newline();
	log.str("Total memory: ").sizeKb(size).newline();
	log.str("Covered memory: ").sizeKb(managedPageCount * PageSize).newline();
	log.str("Buddy table: ").addr(buddyTable).str(" ").sizeb(tableSize);
	log.str(" (").count(tableSizeInPages).str(" pages)").newline();
	log.newline();
}

void PageAllocator::blockSection(void * fromAddress, void * toAddress) {
	void * adjustedFrom = (uint8_t*)start + (((uint8_t*)fromAddress - (uint8_t*)start) & PageMask);
	uint64_t toOffset = (uint8_t*)toAddress - (uint8_t*)start;
	void * adjustedTo = (uint8_t*)start + (toOffset & PageMask) + (toOffset % PageSize == 0? 0 : PageSize);

	blockSection(0, managedPageCount, start, adjustedFrom, adjustedTo);
}

void PageAllocator::blockSection(int index, int nodePageCount, void * currentAddress, void * fromAddress, void * toAddress) {
	//The node is entirely outside the target section
	if ((uint8_t*)currentAddress + nodePageCount * PageSize <= (uint8_t*)fromAddress ||
		(uint8_t*)currentAddress >= toAddress) {
		return;
	//The node is entirely inside the target section
	} else if ((uint8_t*)fromAddress <= (uint8_t*)currentAddress &&
		(uint8_t*)currentAddress + nodePageCount * PageSize <= (uint8_t*)toAddress) {
		BitArray::set(buddyTable, index + PartialOrRequested, false);
		BitArray::set(buddyTable, index + Complete, true);
	//The node is in-between the section's limit
	} else {
		//Indexes of the child nodes
		int leftChildIndex = index * 2 + NodeSize;
		int rightChildIndex = (index + NodeSize) * 2;

		//Memory addresses represented by the child nodes
		void * leftAddress = currentAddress;
		void * rightAddress = (uint8_t*)currentAddress + nodePageCount * PageSize / 2;

		//Recurse
		blockSection(leftChildIndex, nodePageCount / 2, leftAddress, fromAddress, toAddress);
		blockSection(rightChildIndex, nodePageCount / 2, rightAddress, fromAddress, toAddress);

		//Update the node
		bool leftChildComplete = BitArray::get(buddyTable, leftChildIndex + Complete);
		bool rightChildComplete = BitArray::get(buddyTable, rightChildIndex + Complete);
		bool complete = leftChildComplete && rightChildComplete;

		//Update complete bit state
		BitArray::set(buddyTable, index + Complete, complete);
		//Update partial/requested bit state (if not complete, then it's partial, since the node is in-between)
		BitArray::set(buddyTable, index + PartialOrRequested, !complete);
	}
}

void * PageAllocator::alloc(uint64_t requestedPageCount) {
	if (requestedPageCount == 0 || requestedPageCount > managedPageCount)
		return nullptr;

	bool complete = false;
	return allocNode(0, start, managedPageCount, requestedPageCount, complete);
}

void * PageAllocator::alloc(void * requestedAddress, uint64_t requestedPageCount) {
	uint32_t adjustedPageCount = 1;

	//Adjust the requested page count to a power of 2
	while(adjustedPageCount < requestedPageCount)
		adjustedPageCount <<= 1;

	//Adjust the offset between the requested address and the start address to be aligned at
	//adjustedPageCount
	uint64_t offset = (uint8_t*)requestedAddress - (uint8_t*)start;
	offset -= offset % (adjustedPageCount * PageSize);

	//Adjust the requested address
	requestedAddress = (uint8_t*)start + offset;
	return allocNodeAtAddress(0, requestedAddress, adjustedPageCount, start, managedPageCount);
}

void PageAllocator::dealloc(void * address) {
	if (address < start || address > (uint8_t*)start + managedPageCount * PageSize)
		return;

	freeNode(0, address, start, managedPageCount);
}

void * PageAllocator::managedMemoryStart() {
	return start;
}

uint64_t PageAllocator::managedSize() {
	return managedPageCount * PageSize;
}

uint64_t PageAllocator::actualSize() {
	return totalSize;
}

void * PageAllocator::getBuddyTableAddress() {
	return buddyTable;
}

uint32_t PageAllocator::getBuddyTablePageCount() {
	return upperDivide(tableSize, PageSize);
}

void * PageAllocator::allocNode(uint32_t index, void * address, int nodePageCount, int requestedPageCount, bool & complete) {
	bool partial = BitArray::get(buddyTable, index + PartialOrRequested);
	bool currentNodeComplete = BitArray::get(buddyTable, index + Complete);

	//The node is completely used
	if (currentNodeComplete)
		return nullptr;

	//Check if the node fits the request
	if (requestedPageCount <= nodePageCount && requestedPageCount > nodePageCount / 2) {
		//The node fits, but it's partially occupied
		if (partial)
			return nullptr;

		//The node fits and it's free, allocate the memory
		complete = true;
		BitArray::set(buddyTable, index + PartialOrRequested, true);
		BitArray::set(buddyTable, index + Complete, true);

		return address;
	} else {
		//The node doesn't fit the request
		//Prepare the parameters for recursion
		void * result = nullptr;			//Result of the recursion
		bool recursedChildComplete = false;	//Used to calculate if current node should be marked as full

		//Indexes of the child nodes
		int leftChildIndex = index * 2 + NodeSize;
		int rightChildIndex = (index + NodeSize) * 2;

		//Memory addresses represented by the child nodes
		void * leftAddress = address;
		void * rightAddress = (uint8_t*)address + nodePageCount * PageSize / 2;

		//Recurse on the left side
		result = allocNode(
			leftChildIndex, leftAddress, nodePageCount / 2, requestedPageCount,
			recursedChildComplete	//out param
		);

		//Recursion was successful, update the node information and return the allocated address 
		if (result != nullptr) {
			bool rightChildComplete = BitArray::get(buddyTable, rightChildIndex + Complete);
			complete = recursedChildComplete && rightChildComplete;

			//Update complete bit state
			BitArray::set(buddyTable, index + Complete, complete);
			//Update partial/requested bit state (if not complete, then it's partial)
			BitArray::set(buddyTable, index + PartialOrRequested, !complete);
			return result;
		}

		//Recurse on the right side
		result = allocNode(
			rightChildIndex, rightAddress, nodePageCount / 2, requestedPageCount,
			recursedChildComplete	//out param
		);

		//Recursion was successful, update the node information and return the allocated address 
		if (result != nullptr) {
			bool leftChildComplete = BitArray::get(buddyTable, leftChildIndex + Complete);
			complete = leftChildComplete && recursedChildComplete;

			//Update complete bit state
			BitArray::set(buddyTable, index + Complete, complete);
			//Update partial/requested bit state (if not complete, then it's partial)
			BitArray::set(buddyTable, index + PartialOrRequested, !complete);
			return result;
		}

		//No free memory was found on this branch
		return nullptr;
	}
}

void * PageAllocator::allocNodeAtAddress(uint32_t index, void * requestedAddress, int requestedPageCount, void * currentAddress, int nodePageCount) {
	bool partial = BitArray::get(buddyTable, index + PartialOrRequested);
	bool complete = BitArray::get(buddyTable, index + Complete);

	//If any node in the way is complete, then we failed to allocate the requested pages
	if (complete)
		return nullptr;

	//Check if we are at the desired level, if not, recurse
	if (requestedPageCount < nodePageCount) {
		//Memory addresses represented by the child nodes
		void * leftAddress = currentAddress;
		void * rightAddress = (uint8_t*)currentAddress + nodePageCount * PageSize / 2;
		void * result;
		int leftChildIndex = index * 2 + NodeSize;
		int rightChildIndex = (index + NodeSize) * 2;

		//Check on which side we must recurse
		if (requestedAddress < rightAddress) {
			//Recurse on the left side
			result = allocNodeAtAddress(leftChildIndex, requestedAddress, requestedPageCount, leftAddress, nodePageCount / 2);
		} else {
			//Recurse on the right side
			result = allocNodeAtAddress(rightChildIndex, requestedAddress, requestedPageCount, rightAddress, nodePageCount / 2);
		}

		//Check if recursion was successful, update the node information and return the allocated address 
		if (result != nullptr) {
			bool leftChildComplete = BitArray::get(buddyTable, leftChildIndex + Complete);
			bool rightChildComplete = BitArray::get(buddyTable, rightChildIndex + Complete);
			complete = leftChildComplete && rightChildComplete;

			//Update complete bit state
			BitArray::set(buddyTable, index + Complete, complete);
			//Update partial/requested bit state (if not complete, then it's partial)
			BitArray::set(buddyTable, index + PartialOrRequested, !complete);
			return result;
		}

		//No free memory was found on this branch
		return nullptr;
	//Check if we reached the address to be allocated and if the node fits
	} else if (requestedAddress == currentAddress && requestedPageCount == nodePageCount) {
		//Check if the node was used
		if (partial)
			return nullptr;
	
		//Allocate the node
		BitArray::set(buddyTable, index + PartialOrRequested, true);
		BitArray::set(buddyTable, index + Complete, true);

		return currentAddress;
	} else {
		//We should never get here, this is a bug
		log.str("This is a bug in the physical page allocator when allocating a specific memory address.");
		return nullptr;
	}
}

void PageAllocator::freeNode(uint32_t index, void * toFreeAddress, void * currentAddress, int nodePageCount) {
	bool partial = BitArray::get(buddyTable, index + PartialOrRequested);
	bool requested = partial;	//For clarity's sake
	bool complete = BitArray::get(buddyTable, index + Complete);

	//Check if we reached the address to be freed and this was the allocated node
	if (toFreeAddress == currentAddress && complete && requested) {
		//Free the node
		BitArray::set(buddyTable, index + PartialOrRequested, false);
		BitArray::set(buddyTable, index + Complete, false);
	} else if (complete && requested) {
		//The address to be freed belongs to the middle of an allocated block, do nothing and return.
		return;
	} else {
		//If we are on the last level, at this point the page to free either doesn't exist or
		//isn't actually allocated, do nothing and return.
		if (nodePageCount == 1)
			return;

		//Indices of the child nodes
		int leftChildIndex = index * 2 + NodeSize;
		int rightChildIndex = (index + NodeSize) * 2;

		//Memory addresses represented by the child nodes
		void * leftAddress = currentAddress;
		void * rightAddress = (uint8_t*)currentAddress + nodePageCount * PageSize / 2;

		//Check in which node we need to recurse
		if (toFreeAddress < (uint8_t*)rightAddress) {
			//Recurse on the left child
			freeNode(leftChildIndex, toFreeAddress, leftAddress, nodePageCount / 2);
		} else {
			//Recurse on the right child
			freeNode(rightChildIndex, toFreeAddress, rightAddress, nodePageCount / 2);
		}

		//Update this node's state
		bool rightChildComplete = BitArray::get(buddyTable, rightChildIndex + Complete);
		bool leftChildComplete = BitArray::get(buddyTable, leftChildIndex + Complete);
		bool rightChildPartial = BitArray::get(buddyTable, rightChildIndex + PartialOrRequested);
		bool leftChildPartial = BitArray::get(buddyTable, leftChildIndex + PartialOrRequested);
		bool complete = leftChildComplete && rightChildComplete;

		BitArray::set(buddyTable, index + Complete, complete);
		BitArray::set(buddyTable, index + PartialOrRequested, !complete && (
			leftChildComplete || rightChildComplete || leftChildPartial || rightChildPartial
		));
	}
}
