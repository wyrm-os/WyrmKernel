#include "AllocatorSelector.h"
#include "PageAllocator.h"
#include "ChunkAllocator.h"

AllocatorSelector * AllocatorSelector::instance;

AllocatorSelector::AllocatorSelector(PageAllocator & pa, Log & l) : pageAllocator(pa), log(l) {
	instance = this;

	//Allocate a contiguous array of PageSize allocators, that's sizeof(ChunkAllocator) pages.
	allocators = (ChunkAllocator*)pageAllocator.alloc(sizeof(ChunkAllocator));
	log.str("s=").dec(sizeof(ChunkAllocator)).newline();
	log.str("a=").addr(allocators).newline();
	
	//Initialize all allocators, this is not an overkill since they don't initially require any call to the pageAllocator
	for (uint32_t i = 0; i < PageAllocator::PageSize; i++) {
		new(allocators + i) ChunkAllocator(pageAllocator, i, log);
	}
}
